# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 21:41:02 2021

@author: Robin Dohmen
Contact mdohmen1@gwdg.de

This code is part of the Geometry library for python. Reads output files form
Gaussian scans along certain coordinates to generate 1D energy surfaces and
extracts xyz coordinates from the output.
"""

# Import the necessary packages
import sys
import os
import re
import shutil
import numpy as np
from pathlib import Path
from linecache import getline
from warnings import warn

# compiler to get mutlipe floats
p = re.compile(r"[-+]?\d*\.\d+|\d+")

# Import important functions from complementary files
from GeometryClass import Geometry
from spfitExtension import extract_numbers
from masses import AtomicNum2label


def read_Gaussian_scan(filename, deleteXYZfiles=True):
    # format the filename
    log_file = Path(filename).resolve()
    gauss_dir = Path(os.path.splitext(log_file)[0])
    # read the file
    with open(log_file) as gauss_file:
        # optimized orientations
        SecondToLast_opt_idx = []
        # all orientations, not just the optimized one
        orient_idx = []
        # energies of orientations, not just optimized ones
        all_SCF = []
        all_MP2 = []
        # energies of optimized orientations
        energy = []
        MP2_energy = []
        termination = False
        for idx, line in enumerate(gauss_file, 1):
            if 'Standard orientation' in line:
                orient_idx.append(idx)
            if 'SCF Done' in line:
                line = getline(gauss_file.name, idx)
                all_SCF.append(extract_numbers(line)[-2])
            if 'EUMP2' in line:
                line = getline(gauss_file.name, idx)
                NumExtrac = extract_numbers(line)
                all_MP2.append(NumExtrac[4] * 10**NumExtrac[5])
            if 'Optimized Parameters' in line:
                SecondToLast_opt_idx.append(orient_idx[-1])
                energy.append(all_SCF[-1])

                if len(all_MP2) > 0:
                    MP2_energy.append(all_MP2[-1])
            if 'NAtoms' in line:
                line = getline(gauss_file.name, idx)
                natoms = extract_numbers(line)[0]
            if 'ModRedundant input' in line:
                line = getline(gauss_file.name, idx + 1)
                steps = extract_numbers(line)[-2:]
            if 'Normal termination of Gaussian' in line:
                termination = True
        opt_idx = [orient_idx[np.argmin(np.abs(np.array(
                    orient_idx)-el))+1] for el in SecondToLast_opt_idx]
        if len(all_MP2) > 0:
            energy = MP2_energy

        # Checkups
        if termination is False:
            warn(f'{log_file.name}: Gaussian did not terminate normally')

        if steps[0] + 1 != len(energy):
            warn(f'{log_file.name}: Only {len(energy)} out of {steps[0] + 1}' +
                 'scan points were found.')

        # generate numpy array for energy plot
        ScanPlot = np.zeros((len(energy), 2))
        ScanPlot[:, 0] = np.arange(len(energy)) * steps[1]
        ScanPlot[:, 1] = energy

        # make a folder for .xyz files
        try:
            os.mkdir(gauss_dir)
        except FileExistsError:
            pass

        # read optimized coordinates from log files and save them in .xyz files
        for confnum, idx in enumerate(opt_idx):
            coordinates = []
            AtomicNum = []
            for orient_idx in np.arange(idx+5, idx+5+natoms):
                coord_line = getline(gauss_file.name, orient_idx)
                coord_line = extract_numbers(coord_line)
                AtomicNum.append(coord_line[1])
                coordinates.append(coord_line[-3:])
            names = [AtomicNum2label[key] for key in AtomicNum]
            xyz_file = os.path.join(gauss_dir, Path(f'confomer{confnum}.xyz'))
            coordinates = np.array(coordinates)
            with open(xyz_file, 'w', encoding='utf-8') as xyz_file:
                xyz_file.write('{}\n'.format(int(natoms)))
                xyz_file.write(f'Scanpoint: {ScanPlot[confnum, 0]}, '
                               f'Energy: {ScanPlot[confnum, 1]} Hartree\n')
                for lin_idx, name in enumerate(names):
                    name = names[lin_idx].rjust(4, ' ')
                    x = str(coordinates[lin_idx, 0]).rjust(24, ' ')
                    y = str(coordinates[lin_idx, 1]).rjust(24, ' ')
                    z = str(coordinates[lin_idx, 2]).rjust(24, ' ')
                    xyz_file.write(name + x + y + z + '\n')

    # Find paths to generated .xyz files
    files = []
    for path in Path(gauss_dir).rglob('*.xyz'):
        files.append(path.resolve())

    # write energy curve and .xyz files into result file
    xyz_file = os.path.join(gauss_dir, Path(f'confomer{confnum}.xyz'))
    res_file = Path(os.path.abspath(gauss_dir) + '.res')
    with open(res_file, 'w') as res_file:
        for i in ScanPlot:
            res_file.write(f'{i[0]}, {i[1]}\n')
        for i, file in enumerate(files):
            molecule = Geometry(file)
            molecule.compute_ABC_coord()
            xyz_file = os.path.join(gauss_dir, Path(f'confomer{i}.xyz'))
            molecule.create_xyz_file(outputfile=xyz_file,
                                     comment=f' {energy[i]} Hartree')
            xyz_file = open(xyz_file, 'r')
            res_file.write(xyz_file.read())
            xyz_file.close()
    if deleteXYZfiles:
        try:
            shutil.rmtree(gauss_dir)
        except FileNotFoundError:
            pass
    return ScanPlot
