# -*- coding: utf-8 -*-
"""
Created on Mon Aug 15 13:21:38 2022

@author: Beate Kempken
Contact: beate.kempken@stud.uni-goettingen.de

Skript to improve assignments in pickett by searching for wrongly assigned
transitions.
"""

import subprocess
import copy
import shutil
import numpy as np


def find_bad_assignments(filename):
    """
    excludes iteratively one line from the fit-file and makes the fit.
    Prints the fit result for every excluded line.
    at the end: restores original .lin-file with all assignments
    IMPORTANT: put original lin-file and par-file in their respective folders
    before using this function!
    """

    cmd = ['spfit', filename + '.lin', filename + '.par']
    file = open(filename + '.lin', 'r')
    lin_base = []

    # Get the RMS of the fit with all lines included
    subprocess.run(cmd, stdout=subprocess.PIPE)
    shutil.copy('par_files/' + filename + '.par', filename + '.par')

    with open(filename + '.fit', 'r') as f:
        fitfile = f.readlines()
        RMS_original = [float(line[16:32]) for line in fitfile if line.startswith(' MICROWAVE RMS =')]
        RMS_original = min(RMS_original)
    print('initial RMS / MHz=', RMS_original)

    # Now get the RMS of the fit with always one assignment excluded
    for line in file.readlines():
        line = line.strip('\n')
        lin_base.append(line)
    # print(lin_base)

    print('excluded assignment | resulting RMS / MHz')
    for i in range(len(lin_base)):
        lin_old = copy.deepcopy(lin_base)
        removed_line = lin_old[i]
        lin_new = np.delete(lin_old, i)

        np.savetxt(filename + '.lin', lin_new, fmt='%s')

        subprocess.run(cmd, stdout=subprocess.PIPE)
        shutil.copy('par_files/' + filename + '.par', filename + '.par')

        with open(filename + '.fit', 'r') as f:
            fitfile = f.readlines()
            RMS = [float(line[16:32]) for line in fitfile if line.startswith(' MICROWAVE RMS =')]
            RMS = min(RMS)

        print(removed_line, RMS)

    shutil.copy('lin_files/' + filename + '.lin', filename + '.lin')
