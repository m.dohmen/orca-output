# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 11:26:26 2021

@author: Robin Dohmen
Contact mdohmen1@gwdg.de

This code is part of the Geometry library for python. This script handles
interfacing with output from the crest package from the Grimme lab.
https://github.com/grimme-lab/crest
"""

import os
from linecache import getline
import numpy as np
from pathlib import Path
from GeometryClass import Geometry


def find_cutoff(array, value):
    array = np.array(array)
    for idx, energy in enumerate(array):
        if energy < value:
            pass
        else:
            return idx
    return idx


def crest_xyz_to_ORCA(crest_dir, orca_template,
                      command_line=None, orca_inp_name='confomer',
                      additional_commands='', XYZfile=False):
    """
    Create ORCA input file from crest conformer output.
    Read the energy of the confomer in the directory to let the user determine
    a cutoff value or a number of low energy confomers to use and creates a
    the files which can be used as ORCA input based on that.

    Parameters
    ----------
    crest_dir : str
        Path to the directory where crest has been executed and generated its
        its output
    template : str
        Template input file for templating. It has to be a formated ORCA
        input file utilizing cartesian coordinate. For details reference
        the ORCA manual.
    command_line : str, optional
        String for ORCA command line. For details see the ORCA manual.
        Default: given by orca_template
    orca_inp_name : str, optional
        Name for the ORCA input files to be created. Default: confomer
    XYZfile : boolean, optional
        Keep the xyz for each input confomer in a single file.

    Yields
    ------
    Subdirectory at in the crest directory which contains the ORCA input files.
    """
    # read the best crest file
    filename = Path(crest_dir + '\\crest_best.xyz')
    crest_best = Geometry(filename)
    filename = Path(crest_dir + '\\crest_conformers.xyz')
    # Open the confomer file and extract all the energies
    with open(filename) as crest_file:
        counter = 0
        energies = []
        for idx, line in enumerate(crest_file, 1):
            if idx == (2 + counter * (crest_best.natoms + 2)):
                try:
                    energy = float(getline(
                        crest_file.name, 2 + counter * (
                            int(crest_best.natoms) + 2)).strip())
                except ValueError:
                    pass
                energies.append(energy)
                counter += 1
    # Get input from the user for a cutoff value
    energies = np.array(sorted(energies, key=float))
    print('lowest energy conformer:', energies[0])
    print('higest energy conformer:', energies[-1])
    cutoff = input("Enter energy cutoff:")
    try:
        cutoff = float(cutoff.strip())
    except ValueError:
        print('Cutoff not a float, cutoff set to kT')
        cutoff = energies[0] + 9.4316031e-4
    if cutoff < energies[0]:
        print('Cutoff below minimal energy, Orca file will be only created' +
              'for the energetically best conformer')
    cut_idx = find_cutoff(energies, cutoff)
    # confirm choices
    inp = input(f'{cut_idx + 1} confomers chosen. Continue? [y/N]:')
    if np.logical_or('y' == inp, 'Y' == inp):
        pass
    else:
        inp = input('Enter the number of conformers:')
        try:
            inp = int(inp.strip())
            cut_idx = inp - 1
        except ValueError:
            print(f'Input not an integer number, {cut_idx + 1} chosen')

    try:
        os.mkdir(Path(crest_dir + '\\orca_input'))
    except FileExistsError:
        pass
    # Read xyz files and create Orca files by creating temporary xyz files
    # for each confomer
    for idx in np.arange(cut_idx + 1):
        tempfile = Path(crest_dir + f'\\xyz_temp_{idx + 1}.xyz')
        orcafile = Path(crest_dir + f'\\orca_input\\{orca_inp_name}' +
                        str(idx + 1).rjust(len(str(cut_idx+1)), '0') + '.inp')
        line_idx = np.arange(idx * (crest_best.natoms + 2),
                             idx * (crest_best.natoms + 2) + (
                                                     crest_best.natoms + 2))
        with open(filename, 'r') as crest_file, open(
                                                tempfile, 'w') as temp_file:
            for idx2, line in enumerate(crest_file):
                if idx2 in line_idx:
                    temp_file.write(line)
        molecule = Geometry(tempfile)
        if XYZfile:
            os.replace(tempfile, Path(
                          crest_dir + f'\\orca_input\\xyz_temp_{idx + 1}.xyz'))
        else:
            os.remove(tempfile)
        molecule.create_templated_orca_input(template=orca_template,
                                             command_line=command_line,
                                             outputfile=orcafile,
                                             comment=f'{energies[idx]}')
