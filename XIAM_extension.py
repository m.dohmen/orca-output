# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 10:10:39 2023

@author: kempken1

Changes: 

    Make fitting not time-dependent any more but end it after maximum 100 iterations
    
    Allow the neglection of parameter combinations which hang up the program (only very very few but they exist)

"""

import numpy as np
import subprocess
import itertools
import os
import sys
import copy
import shutil
import pandas as pd




# Working from different PCs:
computer =  'Office' #'Laptop'

if computer == 'Laptop':
    Path_until_OwnCloud = 'C:\\Users\\beate\\ownCloud\\'
elif computer == 'Office':
    Path_until_OwnCloud = 'D:\\OwnCloud\\'

sys.path.insert(0, Path_until_OwnCloud + 'Beate\\orca-output-master')
from Number_formatting import val_with_err


# With this routine you run XIAM and update the .xo-file.
def run_XIAM(cwd, xi_name, xo_name):
    '''
    The function calls the software XIAM.
    
    
    
    Parameters:
    ---------
    cwd : string
        The whole path to the XIAM input file
    xi_name : string
        Name of the XIAM input file (with extension)
    xo_name : string
        Name of the XIAM output file (with extension)
    
    Returns
    -------
    None.
    '''
    
    with open(cwd + '\\' + xi_name) as XIAM_input:
        xiam_output = subprocess.run(['XIAM_mod'], stdin = XIAM_input, stdout = subprocess.PIPE, text = True, cwd = cwd).stdout
    
    with open(cwd + '\\' + xo_name, 'w') as outputfile:    
        outputfile.write(xiam_output)
        
        

def read_XIAM_output(cwd, xo_name, params):
    '''
    The function opens a XIAM output fit file (not prediction file) and returns
    the fit results.
    

    Parameters
    ----------
    cwd : string
        Path to the XIAM output file.
    xo_name : string
        Name of the XIAM output file (with extension).
    params : list of strings
        Strings describe all fit parameters.

    Returns
    -------
    params_val : list of floats
        Floats describe the fitted parameter values.
    params_err : list of floats
        Floats describe the errors of the fitted parameter values.
    err_div_val : list of floats
        Floats describe the ratio params_err / params_val.
    terminator : integer
        Indicator variable, either 0 or 1. If terminator = 0: the fit was 
        successful. If terminator = 1: No error could be determined for at 
        least one of the fit parameters.
    std_dev : float
        Standard deviation of the whole fit.
    [BJ, BK, B-] : list of floats
        Fit values for the rotational constants .

    '''

    params_val = []
    params_err = []
    err_div_val = []
    result_lines = []
    
    
    # x serves as variable to extract the textblock in which the fit results are listed.
    x = 0
    with open(cwd+ '\\' + xo_name, 'r') as outputfile:
        lines = outputfile.readlines()
        for line in lines:
            if x == 1:
                result_lines.append(line)
            
            if 'Parameters  and  Errors' in line:
                x = 1
                
            elif 'Standard Deviation' in line:
                std_dev = float(line.split(' ')[-2])
                x = 0
        # Remove the 'Standard Deviation line and the empty line before:
        result_lines = result_lines[:-2] 
        
        # if std_dev not in locals():
        #     print('Something must be wrong with your input file. I could not determine a fit standard deviation.')
        
        terminator = 0
        
        for param in params:
            
            for line in result_lines:
                entries = line.split()

                if param == entries[0]:
                    if '*' in entries[-1][:-1]:
                        terminator = 1
                        continue
                    params_val.append(float(entries[1]))
                    
                    if '}' in entries[-1]:
                        entries[-1] = entries[-1].replace('}', '')
                    
                    if '{' in entries[-1]:
                        entries[-1] = entries[-1].replace('{', '')
                    
                    params_err.append(float(entries[-1])) 
                    
                    
                    err_div_val.append(abs(params_err[-1]) / abs(params_val[-1]))
        
        # extract the rotational constants
        
        for line in result_lines:
            if 'BJ' == line.split()[0]:
                BJ = float(line.split()[1])
                
            elif 'BK' == line.split()[0]:
                BK = float(line.split()[1])
            
            elif 'B-' == line.split()[0]:
                B_minus = float(line.split()[1])
        
        
        return params_val, params_err, err_div_val, terminator, std_dev, [BJ, BK, B_minus]
                



def systematic_XIAM_fitting(cwd, xi_name, params, predicted_params, rel_err_limit, std_dev_max, problematic_combs = []):
    '''
    The function is used for finding a good fit parameter combination for a 
    fit with XIAM. Often it is not obvious which fit parameters might be needed
    for the rotational Hamiltonian and which not. For these cases, in which the 
    fit finding turns out to be a trying around different fit parameter combi-
    nations, systematic_XIAM_fitting might be a good option. 
    The function iteratively prepares the XIAM input file with different fit pa-
    rameter combinations (the input is a list of all possible fit parameters of 
    which all possible permutions are formed) and runs XIAM for each combi-
    nation. Furthermore, it reads in every output fit file and evaluates the 
    fit as a good fit or rejects it, depending on different criteria (see "Pa-
    rameters" below). If a successful fit is found, the corresponding output
    file is saved to a subfolder named "Promising output files". Furthermore,
    the function saves an overview of the successful fits as an excel sheet
    named "Fit results.xslx".
    

    Parameters
    ----------
    cwd : string
        Directory to the folder including the XIAM input file.
    xi_name : string
        Name of the XIAM input file (with extension)
    params : list of strings
        Strings contain fit parameters as they are communicated to XIAM.
    predicted_params: list of strings
        Strings contain those fit params which shall be mentioned in the fit
        block of the XIAM input file and in the prediction block.
    rel_err_limit: float
        Quality criterion for a fit. If the quantity with the highest 
        error/value ratio exceed rel_err_limit, it will be categorized as "bad 
        fit". If not, it will be written to the fit results file.
    std_dev_max : float
        Quality criterion for a fit. If the standard deviation of the whole fit
        exceeds std_dev_max, the fit is considered as being bad and will be re-
        jected.
    tuple_sizes: list or np.array of integer(s)
        integers represent the size of the tuples of parameters which are
        chosen for the fit (allows parallelization of the process)
    problematic_combs (optional) : list of tuples of strings
        A tuple contains a set of strings which represent a certain combination
        of fit parameters. For some fit parameter combinations, it can happen 
        that XIAM hangs up itself (also observable when using XIAM manually). 
        It is highly recommended to evaluate the respective input file careful-
        ly when this happens. Nevertheless, problematic_combs allows to skip 
        such combinations to let the routine come to an end.
        
        
    Returns
    -------
    None.
    
    The function saves a report file and a fit results file to cwd. 
    
    The report file contains all parameter combinations and a comment ("fit bad",
    "fit not possible" or "convergence time limit exceeded"). It also contains 
    the XIAM calculation time.
    The fit results file contains the parameter combinations for which the worst
    determined quantity's error/value ratio is still below rel_err_limit. It 
    also contains the fitted parameter values and their errors.


    Before starting: 
    Make sure to have set a prediction for all parmeters in params, no matter if
    active or commented out.
    (the function doesn't add any lines to the prediction block. It only 
    comments existing lines out or activates them.)
    The different blocks in the input file must be separated by exactly one empty line.
    See the XIAM documentation for the different input blocks.
    
    Make also sure that all parameters in params are mentioned in the fit block.
    Again no matter if active or commented out.
    
    Parameters in the prediction and fit block which are not part of params
    are not touched by the function.
    
    Approximations:
    The function only covers cases in which the predicted parameters in the 
    params are also fitted. It doesn't happen that a fit is run with a part of 
    params being predicted and only a subset of that part being fitted. 
    -> Parameters which are not fittable are approximated to being negligible 
    in the Hamiltonian with the current parameter combination.
    '''
    
    
    xo_name = xi_name[:-3] + '_fit.xo'
    
    if 'Promising output files' not in os.listdir(cwd):
        os.mkdir(cwd + '\\Promising output files')
    else:
        pass
    
    # start pandas dataframe for successful fit results
    Results = pd.DataFrame([], columns = ['Output file'] + params + ['Standard deviation'])
        
    with open(cwd + '\\' + xi_name) as inputfile:
        input_lines = inputfile.readlines()
        
    # check if program is in fit mode. Return warning if it is not
    check = 0
    pred_block_ini = []
    fit_block_ini = []
    
    ###########################################################################
    ######## Extract prediction block and fit block from input file ###########
    ###########################################################################
    
    
    for i in range(len(input_lines)):
        if input_lines[i].startswith('int') and input_lines[i].split()[1] != '0':
            print('''Your input file is not in fit mode. Set "int" in the 2nd \
                  input block to 0 to obtain the fit mode.''')
            sys.exit()
        else:
            pass
        
        if input_lines[i].isspace() == True:
            check = check + 1
            if check == 2:
                start_pred_block = i + 1
            if check == 3:
                start_fit_block = i + 1
            
    # find the prediction parameters block:
        if check == 2:
            pred_block_ini.append(input_lines[i])
        
        if check == 3:
            fit_block_ini.append(input_lines[i])
            
            
        
        if check > 3:
            break
    
    
    pred_block_ini = pred_block_ini[1:]
    fit_block_ini = fit_block_ini[1:]
    
    

    ###########################################################################
    ###### add '\' to the line beginnings of lines containing our params ######
    ###########################################################################


    for i in range(len(pred_block_ini)):
        for param in predicted_params:
            if pred_block_ini[i].startswith('\\'):
                pass
            else:
                if param == pred_block_ini[i].split()[0]:
                    pred_block_ini[i] = '\\' + pred_block_ini[i]
        
        
    for i in range(len(fit_block_ini)):
        for param in params:
            if fit_block_ini[i].startswith('\\'):
                pass
            else:
                if param == fit_block_ini[i].split()[-1]:
                    fit_block_ini[i] = '\\' + fit_block_ini[i]
    
    
    ###########################################################################
    ##### derive prediction block and fit block for every iteration from ######
    #####               pred_block_ini and fit_block_ini                 ######
    ###########################################################################
    
    param_combinations = []
    
    # apparently, itertools.combinations() can only work with at least two 
    # elements. But we also want to iterate through only one and none parameter.
    for i in range(1,len(params)):
        param_combinations = param_combinations + list(itertools.combinations(params, i))


    # Not every combination is possible
    # Remove tuples which contain epsil but not delta:
    
    param_combinations = [Tuple for Tuple in param_combinations if 'epsil' not in Tuple or ('epsil' in Tuple and 'delta' in Tuple)]
    
    
    # Some combinations have shown to lead to a hanging up of XIAM. A list of
    # these combinations is subtracted from the param_combinations list.
    param_combinations = [Tuple for Tuple in param_combinations if Tuple not in problematic_combs]


    with open(cwd  + '\\Report.txt', 'w') as report_file:
        report_file.write(str(len(param_combinations)) + ' combinations\n') 

    # param_combinations = [('DJ', 'DK', 'DJK', 'dk', 'dj', 'V1n', 'delta', 'epsil', 'Dpi2J', 'Dpi2K', 'Dpi2-', 'Dc3J')]
    for idx in range(len(param_combinations)):

        
        fit_block = copy.deepcopy(fit_block_ini)
        pred_block = copy.deepcopy(pred_block_ini)
        
        ## derive prediction block:
        pred_check = 0
        for param in param_combinations[idx]:
            if param not in predicted_params:
                continue
            for line_idx in range(len(pred_block)):
                
                if pred_block[line_idx].startswith('\\') and param == pred_block[line_idx].split()[0][1:]:
                    pred_block[line_idx] = pred_block[line_idx][1:]
                    pred_check = 1
                    break
        
            if pred_check == 0:
                print('Could not prepare the prediction block properly.')
                sys.exit()
        
        ## derive fit block:
        fit_check = 0
        for param in param_combinations[idx]:
            for line_idx in range(len(fit_block)):
                if fit_block[line_idx].startswith('\\') and param == fit_block[line_idx].split()[1]:
                    fit_block[line_idx] = fit_block[line_idx][1:]
                    fit_check = 1
                    break

            if fit_check == 0:
                print('Could not prepare the fit block properly')
                sys.exit()
    # switched param and line_idx for-loops
    # added if pred_block[line_idx].startswith('\\') to derive prediction block

    ###########################################################################
    ######## Save modified input file, run XIAM, and read the output ##########
    ###########################################################################
    
        
        input_lines[start_pred_block : start_pred_block + len(pred_block)] = pred_block
        input_lines[start_fit_block : start_fit_block + len(fit_block)] = fit_block
        
        input_file = ''.join(input_lines)
        
        
        with open(cwd + '\\' + xi_name, 'w') as inputfile:
            inputfile.write(input_file)        
        
        print(param_combinations[idx])
        run_XIAM(cwd, xi_name, xo_name)
        
        params_val, params_err, err_div_val, terminator, std_dev,_ = read_XIAM_output(cwd, xo_name, list(map(str, param_combinations[idx])))
        
        
        
        if len(params_val) == 0:
            with open(cwd + '\\Report.txt', 'a') as report_file:
                report_file.write(', '.join(param_combinations[idx]) + '     fit not possible \n')
            continue
        
        if (max(err_div_val) > rel_err_limit or terminator == 1) and std_dev_max < std_dev:
            with open(cwd  + '\\Report.txt', 'a') as report_file:
                report_file.write(', '.join(param_combinations[idx]) + '     parm error and std deviation too high: param err = ' + '{:.5f}'.format(max(err_div_val)) + ', std_dev = ' + '{:.5f}'.format(std_dev) + '  \n')
            continue
        
        if max(err_div_val) > rel_err_limit or terminator == 1:
            with open(cwd  + '\\Report.txt', 'a') as report_file:
                report_file.write(', '.join(param_combinations[idx]) + '     parm error too high:  param err = ' + '{:.5f}'.format(max(err_div_val)) + '  \n')
            continue
        
        if std_dev_max < std_dev:
            with open(cwd  + '\\Report.txt', 'a') as report_file:
                report_file.write(', '.join(param_combinations[idx]) + '     std deviation too high: std_dev = ' + '{:.5f}'.format(std_dev) + '  \n')
            continue
        
        val_and_err = [None]*(len(params_val) + len(params_err))
        val_and_err[::2] = params_val
        val_and_err[1::2] = params_err
        
        results_line = ', '.join(param_combinations[idx])
        

        for par_idx in range(len(param_combinations[idx])):
            try:
                results_line = results_line + '   ' + str(params_val[par_idx]) + ' \u00B1 ' + str(params_err[par_idx]) 
            except IndexError:
                print('Are you sure, you prepared your input file properly?')
                print('Problematic combination: ', param_combinations[idx])
                sys.exit()
        with open(cwd + '\\Report.txt', 'a') as report_file:
            report_file.write(', '.join(param_combinations[idx]) + ' \n')
        
        
        # Write the results of the successful fit to a text file and save the 
        # corresponding XIAM output file to the "Fit results and report" folder
        # with the parameter combination added to the name.

        # ~ with open(cwd + '\\Fit results and report\\' + 'Fit results.txt', 'a') as results_file:
            # ~ results_file.write(results_line + '\n')
            
        # copy output file to "promising output files" folder and add the
        # parameters to the filename
        
        
        good_comb = ''
        for param in param_combinations[idx]:
            good_comb = good_comb + param + ', '
        good_comb = good_comb[:-2]
        
        
        shutil.copy(cwd + '\\' + xo_name, cwd + '\\Promising output files\\' + xo_name)
        
        outputfile_name = xo_name[:-3] + ' (' + good_comb + ').xo'
        os.rename(cwd + '\\Promising output files\\' + xo_name, cwd + '\\Promising output files\\' + outputfile_name)
        
        
        
        # Combine fit param and value to suitable string
        valerr = val_with_err(params_val, params_err)
        
        # update Results pandas dataframe
        data = [outputfile_name] + valerr + ['{:.5f}'.format(std_dev)]
        cols = ['Output file'] + list(param_combinations[idx]) + ['Standard deviation']
        # print('')
        # print('param_combinations[idx] for results table:', list(param_combinations[idx]))
        # print('')
        appendix = pd.DataFrame([data], columns = cols)
        Results = pd.concat([Results, appendix], ignore_index = True)
        
        # remove XIAM output file from cwd
        # shutil.rmtree(cwd + '\\' + xo_name)        
    Results.to_excel(cwd + '\\Fit results.xlsx')
    print(Results)
# systematic_XIAM_fitting('D:\\OwnCloud\\Research\\fluorobenzaldehydes\\m-fluorobenzaldehyde\\XIAM\\3-t-FBz\\p-H2_o-H2_cav-o-H2\\Manual fitting', '3-t-FBz_XIAM.xi', ['DJ', 'DK', 'DJK', 'dk', 'dj', 'V1n', 'delta', 'epsil', 'Dpi2J', 'Dpi2K', 'Dpi2-', 'Dc3J'], ['V1n', 'delta', 'epsil', 'Dpi2J', 'Dpi2K', 'Dpi2-', 'Dc3J'], [12], 0.1, 1)

def rot_const_converter(rot_constants, conv):
    '''
    The function converts rotational constants from XIAM (GHz) to Pickett (MHz)
    or the other way around.

    Parameters
    ----------
    rot_constants : list of floats
        The rotational constants in MHz (Pickett) or GHz (XIAM).
        
    conv: string
        Conversion direction. Either 'XIAM_to_Pickett' or 'Pickett_to_XIAM'.

    Returns
    -------
    BJ : TYPE
        DESCRIPTION.
    BK : TYPE
        DESCRIPTION.
    B_minus : TYPE
        DESCRIPTION.

    '''
    if conv == 'Pickett_to_XIAM':
        A = rot_constants[0]
        B = rot_constants[1]
        C = rot_constants[2]
    
        BJ = (0.5 * (B + C)) * 10**(-3)
        BK = (A - 0.5 * (B + C)) * 10**(-3)
        B_minus = (0.5 * (B - C)) * 10**(-3)
        return [BJ, BK, B_minus]
        
    elif conv == 'XIAM_to_Pickett':
        BJ = rot_constants[0]
        BK = rot_constants[1]
        B_minus = rot_constants[2]
        
        A = (BJ + BK) * 10**3
        B = (B_minus + BJ) * 10**3
        C = (BJ - B_minus) * 10**3    
        return [A, B, C]
    
    else:
        print('No valid value set for conv!')
        
# def compare_rot_const(rot_const_XIAM, rot_const_Pickett):
#     '''
#     The function compares the rotational constants obtained from XIAM fits with
#     those obtained from a Pickett fit and sorts the XIAM fit results according 
#     to their agreement with the Pickett fit results.

#     Parameters
#     ----------
#     rot_const_XIAM : list of floats
#         Values of BJ, BK and B- from a XIAM fit in GHz.
#     rot_const_Pickett : list of floats
#         Values of A, B and C from a Pickett fit in MHz.

#     Returns
#     -------
#     deviations_sorted : 1D array
#         The first three entries describe the relative difference between XIAM-
#         fitted (BJ, BK, B-) and Pickett-fitted (A, B, C) rotational constants. 
#         The fourth entry describes the sum of these relative deviations. 

#     '''
#     # convert Xiam constants to Pickett constants
#     deviations = np.zeros(4)
    
#     deviations[0] = abs(A_XIAM - A_Pick)/A_Pick
#     deviations[1] = abs(B_XIAM - B_Pick)/B_Pick
#     deviations[2] = abs(C_XIAM - C_Pick)/C_Pick
#     deviations[3] = sum(deviations[:3])

#     return deviations
        
        
        
        
        
        
        
        
            






