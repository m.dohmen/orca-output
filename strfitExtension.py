# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 16:31:24 2021

@author: Robin Dohmen
Contact mdohmen1@gwdg.de

Transformation output of strfit from Pickett's library of rotational
spectroscopy programs. It is also suplemental to the Geometry module, for
batch readout of outputfiles provided.

http://info.ifpan.edu.pl/~kisiel/prospe.htm#table_of_programs

"""

import os
import shutil
import time
import subprocess
from subprocess import Popen
import numpy as np
from GeometryClass import Geometry
from spfitExtension import extract_numbers
import pandas as pd


def create_strfit_from_template(stf_template, orca_file, order=None):
    """
    Create str file from template strfit file with the same geometry and
    parameters.

    The function reads the geometry, calculates the internal coordinates based
    on the ones used in the template. Therefore the order of the atom and the
    geometry file should be the same or the correct order conversion to the
    strfit atom order needs to be supplied. Based on the template a strfit file
    with the same fitting parameters is created.

    Parameters
    ----------
    stf_template : str
        Path to template file with the desired geometry, connectivity and
        parameters.
    orca_file : str
        Path of orca output file or xyz geometry file with another guess
        structure for the geometry fit.
    order : list, optional
        If the order of the atoms in the geometry file doesn't match with the
        template file, they can be reordered.

    Yields
    ------
    Stf file is created and saved in the same directory as the geometry guess.
    """
    # open the strfit template to get the number of atoms and connection matrix
    with open(stf_template) as file:
        data = file.readlines()
    natoms = extract_numbers(data[1])[0]
    connection_matrix = np.zeros((natoms, 3))
    for idx, row in enumerate(connection_matrix):
        connection_matrix[idx] = extract_numbers(data[idx + 2])[1:4]

    # read geometry of ORCA file
    molecule = Geometry(orca_file)
    if molecule.natoms != natoms:
        print('The number of molecules in ORCA file doesn not match the one' +
              'in strfit template')

    # create the name of the strfit file by appending -strfit.stf
    newstffile = os.path.splitext(orca_file)[0] + '-strfit.stf'

    shutil.copy2(stf_template, newstffile)
    with open(newstffile, 'r') as file:
        data = file.readlines()

    if order is not None:
        molecule.reorder_coordinates(order)
    molecule.compute_internal_coordinates(connection_matrix)
    for idx, int_coord in enumerate(molecule.int_coords):
        atnum = str(molecule.atnum[idx]).ljust(2, ' ')
        con1 = str(molecule.connection[idx, 0]).rjust(7, ' ')
        con2 = str(molecule.connection[idx, 1]).rjust(4, ' ')
        con3 = str(molecule.connection[idx, 2]).rjust(4, ' ')
        bon = '{:.7f}'.format(int_coord[0]).rjust(13, ' ')
        ang = '{:.7f}'.format(int_coord[1]).rjust(16, ' ')
        dih = '{:.7f}'.format(int_coord[2]).rjust(15, ' ')
        mass = '{:.7f}'.format(molecule.mass[idx]).rjust(13, ' ')
        data[idx + 2] = (atnum + con1 + con2 + con3 + bon + ang + dih + mass +
                         '\n')

    with open(newstffile, 'w', encoding='utf-8') as file:
        file. writelines(data)
    print(f'.stf file successfully cerated at {newstffile}')
    return newstffile


def submit_multipe_stf_files(stf_template, orca_files, order=None):
    """
    Create and submit multiple stf files for calculation as a subprocess.

    For the creation of the stf files see: create_strfit_from_template.
    Parameters
    ----------
    stf_template : str
        Path to template file with the desired geometry, connectivity and
        parameters.
    orca_file : str
        Path of orca output file or xyz geometry file with another guess
        structure for the geometry fit.
    order : list, optional
        If the order of the atoms in the geometry file doesn't match with the
        template file, they can be reordered.

    Yields
    ------
    Strfit outputfiles for each geometry guess in orca_files.
    """
    for orca_file in orca_files:
        # create the stf file for the calculation
        stf_file = create_strfit_from_template(stf_template, orca_file, order)
        counter = 0
        # path of the outputfile that will be created by strfit
        stf_out = os.path.splitext(stf_file)[0] + '.out'
        # remove outputfile from previous calculation
        try:
            os.remove(stf_out)
        except FileNotFoundError:
            pass
        # I do this in a while loop because for some reason it only works if I
        # Initiate the process twice
        while os.path.exists(stf_out) is False:
            # open strfit in a subprocess, and a PIPE for additional input
            proc = Popen('strfit', stdin=subprocess.PIPE,
                         cwd=os.path.split(stf_file)[0])
            # wait until the process has properly started, otherwise python is
            # to fast
            time.sleep(0.1)
            # Give the input file for strfit the strfit requests
            proc.stdin.write((os.path.splitext(
                            os.path.basename(stf_file))[0] + '\n').encode())
            # safty counter so i don't get stuck in the while loop if something
            # happens
            counter += 1
            if counter > 10:
                break
        # kil the subprocess
        proc.kill()


def multiple_internal_geometry_parameters(stf_template, orca_files,
                                          order=None, outputfile=None):
    """
    Read the bond lengths, angles and dyhedrals in a geometry file based on
    the ones indicated in a strfit template file for comparions between fit and
    quantummechanical calculation.

    stf_template : str
        Path to template file with the desired geometry, connectivity and
        parameters.
    orca_file : str
        Path of orca output file or xyz geometry file with another guess
        structure for the geometry fit.
    order : list, optional
        If the order of the atoms in the geometry file doesn't match with the
        template file, they can be reordered.

    Yields
    ------
    Excel table which lists the bond length, angles and dihedrals of the
    geometry guesses so they can be compared to the fitted results.
    """
    strfit = Geometry(stf_template)
    fit_keys = [key for key in strfit.results_dict.keys() if '(' in key]
    fit_atoms = [extract_numbers(key) for key in fit_keys]
    int_coord = np.zeros((len(orca_files), len(fit_keys)))
    for idx1, file in enumerate(orca_files):
        molecule = Geometry(file)
        if order is not None:
            molecule.reorder_coordinates(order)
        for idx2, fit_at in enumerate(fit_atoms):
            if 'R' in fit_keys[idx2]:
                int_coord[idx1, idx2] = np.linalg.norm(
                            molecule.coordinates[fit_at[0] - 1, :] -
                            molecule.coordinates[fit_at[1] - 1, :])
            if 'A' in fit_keys[idx2]:
                int_coord[idx1, idx2] = molecule._compute_angle(
                            molecule.coordinates[fit_at[0] - 1, :],
                            molecule.coordinates[fit_at[1] - 1, :],
                            molecule.coordinates[fit_at[2] - 1, :])
            if 'D' in fit_keys[idx2]:
                int_coord[idx1, idx2] = molecule._compute_diheral(
                            molecule.coordinates[fit_at[0] - 1, :],
                            molecule.coordinates[fit_at[1] - 1, :],
                            molecule.coordinates[fit_at[2] - 1, :],
                            molecule.coordinates[fit_at[3] - 1, :])

    if outputfile is None:
        outputfile = '{}\\result-intcoord.xlsx'.format(
                        os.path.dirname(orca_files[0]))

    df = pd.DataFrame(data=int_coord, columns=fit_keys)
    df.to_excel(outputfile)
    print(f'Excel table has been printed to {outputfile}')
    return df
