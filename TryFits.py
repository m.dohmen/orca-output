# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 17:12:49 2022

@author: student
"""
import subprocess
import copy
import os
import shutil
# import time
import numpy as np
import sys
import matplotlib.pyplot as plt
import math
from scipy.optimize import curve_fit


def Gaussian(x, amp, mu, sigma):
    return amp / (sigma * np.sqrt(2*math.pi)) * np.exp(-0.5 * ((x-mu)/sigma)**2)


# ~25 datapoints were measured per MHz
def baselinecorrection(cwd, data, delimiter = ' '):
    '''
    The function saves a baseline-corrected version of the spectrum to the
    folder in which the spectrum is stored. The baseline height determination 
    is done for each data point with the 50th percentile over a range of 300 
    data points around it. The name of the file with the baseline-corrected 
    spectrum corresponds to the name of the file with the raw spectrum with the
    extension '_bl' at the end of the file name.
    
    Parameters
    ----------
    cwd : string
        Path to the folder in which the spectrum file is stored.
    data : string
        String consists of name of data file in which the spectrum
        is stored (with extension).       
    delimiter (optional): string
        delimiter between different columns in the data file.

    Returns
    ----------
    None.
    '''

    spectrum = np.loadtxt(cwd + '\\' + data, delimiter = delimiter, unpack=True)
    Percentile = []
    p = 50
    Range = 150

    for i in range(len(spectrum[1])):
        if i < Range:
            Percentile.append(np.percentile(spectrum[1][:i+Range], p))
        elif i < len(spectrum[1])-Range:
            Percentile.append(np.percentile(spectrum[1][i-Range:i+Range], p))
        else:
            Percentile.append(np.percentile(spectrum[1][i-Range:], p))

    spectrum[1] = spectrum[1]-Percentile

    plt.plot(spectrum[0], spectrum[1], 'b-', linewidth=0.5)
    plt.xlabel('Frequency / MHz')
    plt.ylabel('Intensity / mV')
    plt.tight_layout()
    plt.show()

    np.savetxt(cwd + '\\' + data[:-4]+'_bl.dat', np.c_[spectrum[0], spectrum[1]], header='frequency in \
    MHz; intensity in mV')



def blank_spectrum_and_find_peaks(cwd, data, threshold, fit_range=5, nu_limit = 0.05, rms_limit=0.1, echofile='echo.asc'):
    '''
    
    The function removes already assigned peaks from a spectrum by setting 
    their intensity to 0. Furthermore, it extracts the remaining peak frequen-
    cies and writes them to a peaklist which is saved as a text file to a sub-
    folder ('Peaklists') in the folder in which the data file is stored. The 
    name of the text file is the name of the data input file extended by 
    '_peaklist' at the end. 
    
    The neglection of already known peaks occurs in 3 steps:
        
        1. An echo.asc file is read in which contains the pickett-fitted center
           frequencies of already assigned peaks.
        
        2. The itensities of the data points around the identified peak fre-
           quencies are evaluated in view of a continuous intensity decrease 
           when moving away from the center frequency. An intensity increase 
           marks the end of the peak.
        
        3. The intensity of the extracted data points is set to zero.
    
    
    The peak frequencies are determined in two steps:
        
        1. The function iterates through all data point intensities in the 
           blanked spectrum. It identifies points which have an intensity lar-
           ger than the threshold intensity and a higher intensity than their 
           neighbours. The frequencies of these data points are the raw peak 
           frequencies.
    
        2. The raw peak frequencies are refined by fitting a Gaussian to the 
           peak, including 10 data points around the data point with the
           highest intensity. If the fitting is successful, the center frequen-
           cy (one of the fit parameters) is written to the above-mentioned 
           peaklist. If the fitting fails, the corresponding raw peak frequency
           is printed. Possible reasons for a failed fit are... 
           
               1. a RuntimeError
               2. a root-mean-square value which exceeds the limit defined in 
                  advance 
               3. a fitted center frequency which deviates from the raw center 
                  frequency by more than nu_limit given in advance.
           
            
           
    Parameters
    --------
    cwd : string
        Path to the folder in which the spectrum file is stored.
    data: string
        string consists of the name of the file in which the broadband 
        spectrum is stored (with extension)
    threshold: int or float
        minimum intensity value at which a data point can be considered as
        peak and not as noise.
    fit_range (optional) : integer
        Parameter for a Gaussian fit to a peak: fit_range gives the number of 
        data points on the low / high frequency side of the peak center, which shall be in-
        cluded in the fit.
    nu_limit (optional) : float or integer
        Indicator of quality of a Gaussian fit to a peak: The fit is considered 
        to be usable if its center frequency result does not deviate by more 
        than nu_limit from the respective raw peak frequency.
    rms_limit (optional) : float or integer
        Indicator of quality of a Gaussian fit to a peak: The fit is considered 
        to be usable if its root-mean-square value is smaller than rms_limit.
    echofile (optional): string
       string consists of the name of the .asc file which contains the 
       center frequencies of already assigned peaks.

    Returns
    --------

    '''

    # load the text file which contains the broadband spectrum
    spectrum = np.loadtxt(cwd + '\\' + data, unpack=True, skiprows=1)

    print("Shall already assigned peaks be removed from the baseline-corrected spectrum to create a reduced list of peak frequencies? (answer 'yes' or 'no')")
    Q = input()

    # load the echo.asc file which contains the frequencies of the peaks
    # which have already been assigned
    if Q == 'yes' or Q == 'Yes':
        echo = np.loadtxt(cwd + '\\' + echofile, usecols=0, unpack=True)

        ####################################################################
        #########     Remove known peaks from spectrum     #################
        ####################################################################

        # create an array which will be filled with the center frequencies
        # of already assigned peaks in the following for-loop
        known_peaks = np.zeros((2, len(echo)), dtype=object)

        for i, known_peak in enumerate(echo):
            Idx = (np.abs(spectrum[0] - known_peak)).argmin()
            known_peaks[0][i] = float(spectrum[0][Idx])
            known_peaks[1][i] = int(Idx)

        # remove remaining zeros from known_peaks array
        p = 0
        while p < len(known_peaks):
            if known_peaks[0][p] == 0:
                known_peaks = np.delete(known_peaks, p, 1)
            else:
                p = p+1

        # set the intensity of data points which are part of a known peak to
        # zero
        for i in range(len(known_peaks[0])):
            j = 0
            k = 0
            while spectrum[1][known_peaks[1][i]-(j+1)] < spectrum[1][known_peaks[1][i]-j]:
                j = j+1
            while spectrum[1][int(known_peaks[1][i]+(k+1))] < spectrum[1][int(known_peaks[1][i]+k)]:
                k = k+1

            spectrum[1][known_peaks[1][i]-j:known_peaks[1][i]+k] = 0
        #np.savetxt(data + '_bl_blanked.txt', np.c_[spectrum[0], spectrum[1]])

    ####################################################################
    ##############    Find peaks in remaining dataset     ##############
    ####################################################################

    # create an array which will be filled with the center frequencies
    # of the unassigned peaks in the spectrum
    peaklist = np.zeros(len(spectrum[0]))
    peak_Intensities = np.zeros(len(spectrum[0]))

    
    failed_fit = 0
    diff_peak_maxima = []
    failed_fit_rms = []
    failed_frequencies = []
    
    for i in range(1, len(spectrum[1])-1):
        if spectrum[1][i] > threshold and spectrum[1][i-1] < spectrum[1][i] and spectrum[1][i] > spectrum[1][i+1]:
            
            try:
                popt, pcov = curve_fit(Gaussian, spectrum[0][i-fit_range:i+fit_range], spectrum[1][i-fit_range:i+fit_range], p0=[0.0001,spectrum[0][i],1])
            
            except RuntimeError:
                failed_fit = failed_fit + 1
                failed_frequencies.append(spectrum[0][i])
                
            rms = np.sqrt(sum((spectrum[1][i-fit_range:i+fit_range]-Gaussian(spectrum[0][i-fit_range:i+fit_range], *popt))**2))
            # print('rms = ',rms)
            nu = np.linspace(spectrum[0][i-fit_range],spectrum[0][i+fit_range], 100)            
            
            plt.plot(nu, Gaussian(nu, *popt), 'r-')

            
            if abs(popt[1]-spectrum[0][i]) > nu_limit or rms >= rms_limit:
                failed_fit = failed_fit + 1
                failed_frequencies.append(spectrum[0][i])
                failed_fit_rms.append(rms)
                diff_peak_maxima.append(abs(popt[1]-spectrum[0][i]))

            else:
                peaklist[i] = popt[1]
            

    peaklist = peaklist[peaklist != 0]
    peak_Intensities = peak_Intensities[peak_Intensities != 0]
    
    plt.plot(spectrum[0], spectrum[1], 'k.')
    plt.show()
    
    print('')
    print('Failed frequencies:')
    print(failed_frequencies)
    print('')
    print('rms of failed fits:')
    print( failed_fit_rms)
    print('')
    print('difference in experimental and fitted peak maxima:')
    print(diff_peak_maxima)
    print('')
    print('Number of failed fits:')
    print(failed_fit)
    print('')
    

    #print("Do you need a plot of the spectrum with the extracted peaks being marked? (answer 'yes' or 'no')")
    #Figure = input()
    #
    # if Figure == 'yes' or Figure == 'Yes':
    
    # append Peaklists folder to cwd
    
    if 'Peaklists' not in os.listdir(cwd):
        os.mkdir(cwd + '\\' + 'Peaklists')
    
    cwd = cwd + '\\' + 'Peaklists\\'

    
    if Q == 'yes' or Q == 'Yes':
        np.savetxt(cwd + data[:-4] + '_blanked_peaklist' + '_' + str(threshold) + 'mV.txt',
                   peaklist, header='frequency in MHz', fmt='%.6f')
        print('A text file containing the center frequencies of all peaks in the reduced spectrum was created.')

    elif Q == 'no' or Q == 'No':
        np.savetxt(cwd + data[:-4] + '_peaklist' + '_' + str(threshold) + 'mV.txt',
                   peaklist, header='frequency in MHz', fmt='%.6f')
        print('A text file containing the center frequencies of all peaks in the spectrum was created.')

    else:
        print("Spelling 'yes' or 'no' correctly is not that difficult... :P")
    


def StartFit(cwd, filename, peaklist_name, Range, Range_test, RMS_max, QN_main,
             QN_validate):
    '''
    The function initiates a pickett fit to a rotational spectrum for a species
    for which a theoretical prediction of the rotational constants already
    exists. The fit finding is based on 3 main transitions and a set of test
    transitions (number of the latter is freely choosable).
    
    The function works in x steps:
        1. Seach windows around the predicted frequencies of the 3 main transi-
           tions are defined: The functional argument 'Range' contains estima-
           ted errors of each predicted rotational constant value. On this 
           basis, rotational model spectra of the species are predicted for
           a set of rotational constants at their lowest and highest possible 
           values, respectively. With this, for each main transition a low and 
           a high frequency limit can be determined. In the following, frequen-
           cies from the peaklist (-> peaklist_name) which are in one of these 
           frequency windows are collected. In this way, a set of peak frequen-
           cies for each main transition is obtained.
           
        2. Set up the pickett .lin file with the main transitions being 
           assigned to the lowest frequency peak of their respective peak set.
           
           SPFIT and SPCAT are run to obtain the adapted model spectrum. The 
           predicted frequencies of the test transitions are extracted from the
           .cat file. Test windows around these frequencies (defined by 
           Range_test) are calculated. If all of them cover at least one peak,
           each test transition is assigned to the peak frequency which has the 
           smallest deviation from the predicted transition frequency. SPFIT is
           run again. This time, the root-mean-square value of the fit is read
           out from the .fit file. If it is smaller than RMS_max and if all
           fit values for the rotational constants are positive, the rms value,
           the fitted constants and the transition frequencies are saved to a
           results text file and the routine prints 'I found something! :)'.
           Then, the next tuple of main transition frequencies is chosen and 
           the routine goes through this section again. If the aforementioned 
           conditions are not met, the routine directly chooses a new main 
           transition frequency tuple.
    
    
    
    Parameters
    ----------
    cwd : string
        Current working directory. Put '\\' at the end.
    
    filename : string
        Name of the species (=name of the .par and .lin file)

    peaklist_name : string
        Name of the peaklist (with extension).

    Range : list of integers or floats
        List of uncertainties (in MHz) for each fit parameter in the .par file. The
        first list element corresponds to the uncertainty of the first fit
        parameter in the file and so on. 
        The routine uses two categories of transitions: main transitions and 
        test transitions. Three strong transitions are chosen as main
        transitions. They are used for the primary fitting. Test transitions 
        are used to validate the primary fits via secondary fitting.
        The fit parameter uncertainty is transferred to a transition frequency
        uncertainty - creating an individual frequency "window" around each 
        predicted main transition frequency.
        
    Range_test : int or float
        Frequency search window (in MHz) around predicted test transition frequencies 
        after primary fit. Typically around 1 MHz.

    RMS_max: int or float
        Maximum RMS value of a fit to be considered as a usable fit.

    QN_main: list of 3 lists of 2 strings
        Each of the 3 sublists corresponds to a main transitions. The two 
        strings contain the quantum numbers of the final (f) and the initial (i)
        rotational state. A sublist has the form ['J_f Ka_f Kc_f', 'J_i Ka_i Kc_i'].
        
        
    QN_validate: list of n lists of 2 strings
        n is the number of test transitions. It can be chosen freely. The format
        is the same as for QN_main.
        
        

    Returns
    -------
    a text file '<filename>_fit_results.txt' with fit results. The file is 
    saved to the current folder.

    Preparation
    -------
    Make sure that the .lin file contains 3 assignments and that the .par file
    only contains the three rotational constants and no centrifugal distortion 
    constants.

    '''

    # tell the system the current path so that spfit and spcat know where to be run
    os.chdir(cwd)

    # make lin_files and par_files folder in which the original .lin and .par
    # file are saved.
    if 'lin_files' and 'par_files' not in os.listdir(cwd):
        os.mkdir(cwd + 'lin_files')
        os.mkdir(cwd + 'par_files')

        shutil.copy(cwd + filename + '.par', cwd +
                'par_files\\' + filename + '.par')
        shutil.copy(cwd + filename + '.lin', cwd +
                'lin_files\\' + filename + '.lin')

    shutil.copy(cwd + filename + '.par', cwd +
                            'par_files\\' + filename + '.par')
    shutil.copy(cwd + filename + '.lin', cwd +
                            'lin_files\\' + filename + '.lin')

    # define the spfit and spcat commands
    cmd = ['spfit', filename + '.lin', filename + '.par']
    cmd2 = ['spcat', filename + '.var', filename + '.par', filename + '.int']
    
    # equate .var and .par file and run SPCAT to create DFT-prediction spectrum
    with open(cwd + filename + '.par') as par_file:
        par_file_lines = par_file.readlines()
        for i in range(3,len(par_file_lines)):
            if ' 10000 ' in par_file_lines[i]:
                A_calc = par_file_lines[i].split()[1]
            if ' 20000 ' in par_file_lines[i]:
                B_calc = par_file_lines[i].split()[1]
            if ' 30000' in par_file_lines[i]:
                C_calc = par_file_lines[i].split()[1]
    
    
    
    # Open original .var file
    with open(cwd + filename + '.var', 'r') as var_file:
        var_file_lines = var_file.readlines()

    subprocess.run(cmd2, stdout=subprocess.PIPE, cwd=cwd)
    
    
    # Replace A, B and C values by their highest possible values
    with open(cwd + filename + '.var', 'w') as var_file:
        for i in range(len(var_file_lines)):
            if ' 10000 ' in var_file_lines[i]:                
                var_file_lines[i] = var_file_lines[i].replace(var_file_lines[i].split()[1], str(float(A_calc) + Range[0]))
            if ' 20000 ' in var_file_lines[i]:
                var_file_lines[i] = var_file_lines[i].replace(var_file_lines[i].split()[1], str(float(B_calc) + Range[1]))
            if ' 30000 ' in var_file_lines[i]:
                var_file_lines[i] = var_file_lines[i].replace(var_file_lines[i].split()[1], str(float(C_calc) + Range[2]))
            
            var_file.writelines(var_file_lines[i])  
    subprocess.run(cmd2, stdout=subprocess.PIPE, cwd=cwd)
    
    
    
    # Extract main transition frequencies from .cat file
    nu_main_max = np.zeros(len(QN_main))
    nu_main_min = np.zeros(len(QN_main))
    
    for v in range(len(QN_main)):
        with open(cwd + filename + '.cat', 'r') as cat:
            for line in cat.readlines():
                if QN_main[v][0] in line and QN_main[v][1] in line:
                    nu_main_max[v] = (float(line[:13]))

    # Replace A, B and C values by their lowest possible values
    with open(cwd + filename + '.var', 'w') as var_file:
        for i in range(len(var_file_lines)):
            if ' 10000 ' in var_file_lines[i]:                
                var_file_lines[i] = var_file_lines[i].replace(var_file_lines[i].split()[1], str(float(A_calc) - Range[0]))
            if ' 20000 ' in var_file_lines[i]:
                var_file_lines[i] = var_file_lines[i].replace(var_file_lines[i].split()[1], str(float(B_calc) - Range[1]))
            if ' 30000 ' in var_file_lines[i]:
                var_file_lines[i] = var_file_lines[i].replace(var_file_lines[i].split()[1], str(float(C_calc) - Range[2]))
            
            var_file.writelines(var_file_lines[i])  
                    
    subprocess.run(cmd2, stdout=subprocess.PIPE, cwd=cwd)
    
    for v in range(len(QN_main)):
        with open(cwd + filename + '.cat', 'r') as cat:
            for line in cat.readlines():
                if QN_main[v][0] in line and QN_main[v][1] in line:
                    nu_main_min[v] = (float(line[:13]))
                    

    
    

    

    # load the peaklist of the broadband spectrum
    peaklist = np.loadtxt(cwd + 'Peaklists\\' + peaklist_name, unpack=True)

    # define 1D arrays with peak frequencies in the spectral window around
    # predictions nu1, nu2 and nu3
    peaks_1 = np.array(
        [x for x in peaklist if x > nu_main_min[0] and x < nu_main_max[0]])
    peaks_2 = np.array(
        [x for x in peaklist if x > nu_main_min[1] and x < nu_main_max[1]])
    peaks_3 = np.array(
        [x for x in peaklist if x > nu_main_min[2] and x < nu_main_max[2]])


    # definition of an empty array which will be filled with the fit results at
    # a later point
    best_results = np.zeros((100, 7+len(QN_validate)))
    
    
    
    # Define headline of fit_results file ( file which contains best_results
    # and will be created at the end of the script):
    headline = 'RMS /MHz  ' \
               + 'nu_%s<-%s /MHz  ' % (QN_main[0][0].replace(" ", ""), QN_main[0][1].replace(" ", ""))  \
               + 'nu_%s<-%s /MHz  ' % (QN_main[1][0].replace(" ", ""), QN_main[1][1].replace(" ", ""))  \
               + 'nu_%s<-%s /MHz  ' % (QN_main[2][0].replace(" ", ""), QN_main[2][1].replace(" ", ""))
               
    for i in range(len(QN_validate)):
        headline = headline + 'nu_%s<-%s/MHz  ' % (QN_validate[i][0].replace(" ", ""), QN_validate[i][1].replace(" ", ""))

    headline = headline + 'A /MHz  B /MHz  C /MHz'




    # indicator variable for (continue script / stop script)-decision
    check_total= 0
    
    print('frequency window ',QN_main[0][0], ' <- ', QN_main[0][1],  ': ', str(nu_main_min[0]), 'MHz to ', str(nu_main_max[0]), 'MHz' )
    print('frequency window ',QN_main[1][0], ' <- ', QN_main[1][1],  ': ', str(nu_main_min[1]), 'MHz to ', str(nu_main_max[1]), 'MHz' )
    print('frequency window ',QN_main[2][0], ' <- ', QN_main[2][1],  ': ', str(nu_main_min[2]), 'MHz to ', str(nu_main_max[2]), 'MHz' )
    print('')

    print('lengths of the peaklists peaks_1, peaks_2 and peaks_3:', 
          len(peaks_1), len(peaks_2), len(peaks_3))
    print('')
    combinations = len(peaks_1) * len(peaks_2) * len(peaks_3)
    # Estimation of the calculation time: A former calculation needed 2163 sec
    # for 50996 combinations
    seconds = combinations * 2163/50996
    print("Estimated calculation time on Beate's PC:", str(int(seconds // 3600)), 'hours ', str(int((seconds % 3600) // 60)) ,'minutes', str(int((seconds % 60))), 'seconds')
    print("Do you want to proceed? -> type <Enter> or 'no'")
    enter = input()
    
    if enter == 'no':
        check_total = 1
    else:
        pass
    
    # counter for the lines in the result file (always increases by 1 as soon as
    # a new line has been added)
    x = 1
    

    # open the backup .lin file and store its content in
    lin_in = open(cwd + 'lin_files\\'+filename + '.lin', 'r')

    lin_original = []
    for line in lin_in.readlines():
        values = line.split()
        lin_original.append(values)
    lin_in.close()

    # Test if lin_original has 3 assigments:
    if len(lin_original) > 3:
        print('\033[93m'+ 'Warning: Your .lin-file contains more than 3 assignments!' + '\033[0m')
        check_total = 1
    elif len(lin_original) < 3:
        print('\033[93m'+ 'Warning: Your .lin-file contains less than 3 assignments!' + '\033[0m')
        check_total = 1
    else:
        pass
    
    # iterate through all combinations of assignments which are possible with the
    # frequencies in peaks_1, peaks_2 and peaks_3
    for i in peaks_1:
        if check_total == 1:
            shutil.rmtree(cwd + 'lin_files')
            shutil.rmtree(cwd + 'par_files')
            sys.exit()
        print('i=', i)
        for j in peaks_2:
            for k in peaks_3:
                


                    
                # indicator variable for (continue current assignment combination /
                # get to next assignment combination)-decision                    
                check = 0
                
                # Write the new assignment to the .lin file
                counters = np.array([i, j, k])

                lin = copy.deepcopy(lin_original)

                for y in range(len(lin_original)):   # should contain 3 lines
                    tmp = 7-len(str(int(counters[y])))
                    whitespace = ' '*tmp
                    lin[y][0] = ' '*2 + lin_original[y][0]
                    lin[y][-3] = whitespace + str('{:.6f}'.format(counters[y]))
                    lin[y][-2] = ' '*3 + lin_original[y][-2]
                    lin[y][-1] = ' ' + lin_original[y][-1]

                # save the updated .lin file
                np.savetxt(cwd + filename + '.lin', lin,
                           fmt='%s', delimiter='  ')

                # Run SPFIT and SPCAT and restore .par-file
                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)
                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)
                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)

                subprocess.run(cmd2, stdout=subprocess.PIPE, cwd=cwd)
                shutil.copy(cwd + 'par_files\\' + filename +
                            '.par', cwd + filename + '.par')

                # open .cat file and save the frequency of the test transitions
                # in nu_test_cat

                nu_test_cat = []

                for v in range(len(QN_validate)):
                    cat = open(cwd + filename + '.cat', 'r')
                    for line in cat.readlines():
                        if QN_validate[v][0] in line and QN_validate[v][1] in line:
                            nu_test_cat.append(float(line[:13]))
                    cat.close()
                
                # define 1D array which contains frequencies of peaks in the
                # test frequency window around the QN_validate frequencies
                nu_test_exp = []   # list which will be filled with closest test transition frequencies in bb spectrum

                for v in range(len(nu_test_cat)):
                    window = np.array(
                        [i for i in peaklist if abs(nu_test_cat[v] - i) < Range_test])
                    if len(window) > 0:
                        Idx = np.abs(window - nu_test_cat[v]).argmin()
                        nu_test_exp.append(window[Idx])

                # if no transition is found, the fit is wrong and we can pass to another combination.
                    else:
                        check = 1
                        break

                
                # It can happen that the fit is so bad that the test transitions
                # are not amymore listed in the .cat file. In this case we can 
                # continue to another combination
                if len(nu_test_cat) < len(QN_validate):
                    check = 1
                
                
                if check == 1:
                    continue
                
                # Rewrite the .lin file - now including the extracted QN_validate
                # frequencies
                for v in range(len(nu_test_exp)):
                    digits = len(str(int(nu_test_exp[v])))

                    with open(cwd + filename + '.lin', 'a+') as lin_mod:
                        lin_mod.write('  ' + QN_validate[v][0][0]+'  ' + QN_validate[v][0][2] + '  '
                                      + QN_validate[v][0][4] + '  ' + QN_validate[v][1][0] + '  ' + QN_validate[v][1][2] +
                                      '  ' + QN_validate[v][1][4] +
                                      '  0  0  0  0  0  0' + (9-digits)*' '
                                      + str(nu_test_exp[v]) + '     0.010000   1.00000' + '\n')

                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)
                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)
                shutil.copy(cwd + 'par_files\\' + filename +
                            '.par', cwd + filename + '.par')
                np.savetxt(cwd + filename + '.lin', lin,
                           fmt='%s', delimiter='  ')

                # read .fit-file: extract RMS, A, B and C
                with open(cwd + filename + '.fit', 'r') as f:
                    fitfile = f.readlines()
                    
                    for line in fitfile:
                        if 'NEXT LINE NOT USED IN FIT' in line:
                            check = 1
                            break
                     
                    if check == 1:
                        continue
                    
                    
                    RMS = [float(line[16:32]) for line in fitfile
                           if line.startswith(' MICROWAVE RMS =')]
                    RMS = min(RMS)  # only the last RMS value is relevant
                    # as it is the one at which the RMS has
                    # converged to a minimum value

                    A = [float(line[29:46]) for line in fitfile
                         if '   10000          A    ' in line]
                    A = A[-1]
                    B = [float(line[29:46]) for line in fitfile
                         if '   20000          B    ' in line]
                    B = B[-1]
                    C = [float(line[29:46]) for line in fitfile
                         if '   30000          C    ' in line]
                    C = C[-1]



                # writes fits with low RMS and write their results
                # to a _fit_results.txt file

                frequencies = [i, j, k] + nu_test_exp

                if RMS < RMS_max and abs(A-float(A_calc)) < Range[0] and abs(B-float(B_calc)) < Range[1] and abs(C-float(C_calc)) < Range[2] and len(frequencies) == len(set(frequencies)):
                    for v in range(len(nu_test_exp)):
                        try:
                            best_results[x][0] = RMS
                            best_results[x][1] = i
                            best_results[x][2] = j
                            best_results[x][3] = k
                            best_results[x][4+v] = nu_test_exp[v]
                            best_results[x][-3] = A
                            best_results[x][-2] = B
                            best_results[x][-1] = C
                        except IndexError:
                            print('Fit_results file is full.')
                            check_total = 1
                            
                        if check_total == 1:
                            shutil.rmtree(cwd + 'lin_files')
                            shutil.rmtree(cwd + 'par_files')
                            sys.exit()
                    
                    x = x+1
                    np.savetxt(cwd + filename + '_fit_results.txt',
                               best_results[1:], header = headline,
                    fmt='%.6f, %.6f, %.6f, %.6f,' + len(nu_test_exp) * ' %.6f,' + ' %.5f, %.5f, %.5f')
                    print('I found a good fit! :)')

    shutil.copy(cwd + 'lin_files\\' + filename +
                '.lin', cwd + filename + '.lin')


    
    shutil.rmtree(cwd + 'lin_files')
    shutil.rmtree(cwd + 'par_files')

# StartFit('D:\\OwnCloud\\Research\\fluorobenzaldehydes\\p-fluorobenzaldehyde\\pickett\\','4-FBz-H2-O-bound', '4FBz-H2_23052022_25C-3barNe_23052022_2__3900k_FT_avg_blanked_peaklist_0.0006mV.txt',  [30,1,1], 5, 0.03, [['3 1 3', '2 1 2'], ['3 0 3','2 0 2'], ['3 1 2', '2 1 1']], [['4 1 4', '3 1 3'], ['4 0 4', '3 0 3'], ['4 1 3', '3 1 2']])
# StartFit('D:\\OwnCloud\\Research\\1,1-difluorobutene\\Pickett\\','1DFB', 'FULL SPECTRUM_11DFBe_5Mil_RT_bl_blanked_peaklist_0.0002mV.txt',  [150,25,25],
          # 5, 0.3, [['6 1 6', '5 1 5'], ['6 0 6','5 0 5'], ['6 1 5', '5 1 4']], [['7 1 7', '6 1 6'], ['7 0 7', '6 0 6']])#, ['7 2 6', '6 2 5']
# StartFit('D:\\OwnCloud\\Research\\1,1-difluorobutene\\Pickett\\','3DFB', 'FULL SPECTRUM_11DFBe_5Mil_RT_bl_blanked_peaklist_0.0002mV.txt',  [150,20,20],
#             5, 0.3, [['6 1 6', '5 1 5'], ['6 0 6','5 0 5'], ['6 1 5', '5 1 4']], [['7 1 7', '6 1 6'], ['7 0 7', '6 0 6']])#, ['7 2 6', '6 2 5']
# StartFit('D:\\OwnCloud\\Research\\1,1-difluorobutene\\Pickett\\','3DFB', 'FULL SPECTRUM_11DFBe_5Mil_RT_bl_blanked_peaklist_0.0002mV.txt',  [150,25,25],
         # 5, 0.2, [['6 1 6', '5 1 5'], ['6 0 6','5 0 5'], ['6 1 5', '5 1 4']], [['7 1 7', '6 1 6'], ['7 0 7', '6 0 6']])#, ['7 2 6', '6 2 5']
# StartFit('D:\\OwnCloud\\Research\\furan h2\\Pickett\\', 'F-(o-H2)', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_blanked_peaklist_2.5e-05mV.txt',
         # [150, 20, 20], 1, 0.1, [['2 1 2', '1 1 1'], ['2 0 2', '1 0 1'], ['2 1 2', '1 1 0']], [['5 1 4', '5 1 5'],['6 1 5', '6 1 6']])
# StartFit('D:\\OwnCloud\\Research\\pentafluorobutene-1\\Pickett\\','1PFB', 'FULL SPECTRUM_11DFBe_5Mil_RT_bl_blanked_peaklist_0.0002mV.txt',  [150,20,20],
#             5, 0.3, [['6 1 6', '5 1 5'], ['6 0 6','5 0 5'], ['6 1 5', '5 1 4']], [['7 1 7', '6 1 6'], ['7 0 7', '6 0 6']])#, ['7 2 6', '6 2 5']








