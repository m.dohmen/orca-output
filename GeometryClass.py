# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 11:52:45 2021

@author: Robin Dohmen
Contact mdohmen1@gwdg.de

This code is part of the Geometry library for python. It contains the main
class which allows the input of cartesian coordinates in various formats and
allows various transformations required for everyday use in rotational
spectroscopy.
For more details refer to the docstring.
"""
import shutil
import re
import os
import itertools
import scipy
import numpy as np
import pandas as pd
from pathlib import Path
from collections import defaultdict
from linecache import getline
from warnings import warn
from scipy.constants import e
from masses import masses, nearest_key, BaileyConversion
from spfitExtension import extract_numbers

au = scipy.constants.physical_constants[
    'atomic unit of electric field gradient'][0]

# compiler to get mutlipe floats
p = re.compile(r"[-+]?\d*\.\d+|\d+")

# convertions factor for rotational constant to MHz
conv = 505379.0091

# mapping scheme for eigenvectors
eigenv_map_sheme = {
                    'default': [1, 0, 2],
                    'Br': [2, 1, 0],
                    'BR': [2, 1, 0],
                    'Cl': [1, 0, 2],
                    'CL': [1, 0, 2],
                    'I': [0, 1, 2]
                    }


class Geometry:
    """Reads holds and transforms molecular geometry."""

    def __init__(self, filename, **kwargs):
        self.filename = filename
        self.com = np.zeros(3)
        self._categorize_file()
        if self.file_type == 'Orca 3 SP':
            self._read_Orca4_SP_output()
        elif self.file_type == 'Orca 3 Geometry Opt':
            self._read_Orca4_OPT_output()
        elif self.file_type == 'Orca 4 SP':
            self._read_Orca4_SP_output()
        elif self.file_type == 'Orca 4 Geometry Opt':
            self._read_Orca4_OPT_output()
        elif self.file_type == 'Orca 5 SP':
            self._read_Orca5_SP_output()
        elif self.file_type == 'Orca 5 Geometry Opt':
            self._read_Orca5_OPT_output()
        elif self.file_type == 'Orca 5 Multiple SP':
            self._read_multiple_Orca5_SP_output(**kwargs)
        elif self.file_type == '.xyz file':
            self._read_xyz_file()
        elif self.file_type == 'Strfit output':
            self._read_strfit_output()
        else:
            warn('Orca filetype not recognized')

    def __str__(self):
        """Return the filepath for saving and loading file."""
        return str(self.filename)

    def __repr__(self):
        """Return the filepath for saving and loading file."""
        return str(self.filename)

    def print_coords(self):
        """Print the atom number, element, cartesian coordinates and mass.

        Preferably the coordinates in the principal axis system are printed.
        """
        if hasattr(self, 'coord_ABC'):
            for idx, name in enumerate(self.names):
                print(self.atnum[idx], self.names[idx], self.coord_ABC[idx, :],
                      self.mass[idx])
        else:
            for idx, name in enumerate(self.names):
                print(self.atnum[idx], self.names[idx],
                      self.coordinates[idx, :], self.mass[idx])

    def modify_mass(self, mod_atnum=None, mod_mass=None):
        """Allow the modification of an atom's mass to consider isotopes."""
        if mod_atnum is None:
            self.print_coords()
            mod_atnum = input("Enter atom number to be modified:")
            atidx = np.where(self.atnum == int(mod_atnum))[0][0]
            mod_mass = input("Enter the new mass:")
        else:
            atidx = np.where(self.atnum == int(mod_atnum))[0][0]
        try:
            mod_mass = masses[mod_mass]
            self.mass[atidx] = mod_mass
        except KeyError:
            try:
                mod_mass = float(mod_mass.strip())
                self.mass[atidx] = mod_mass
            except ValueError:
                print('Key not available. Enter the key with the mass number' +
                      ' and the elemental symbol without a space eg. 13C')
        self.print_coords()

    def compute_COM(self):
        """Calculate the center of mass of the geometry."""
        com = np.dot(self.mass, self.coordinates) / np.sum(self.mass)
        self.com = com
        return self.com

    def compute_planar_Inertia_tensor(self):
        """Calculate the planar inertia tensor in amu*A**2 .

        Based on https://github.com/smparker/orient-molecule
        """
        self.compute_COM()
        data = self.coordinates - self.com
        self.planar_inertia_tensor = np.einsum(
            "ax,a,ay->xy", data, self.mass, data)
        return self.planar_inertia_tensor

    def compute_rotational_constants(self):
        """Compute the rotational constants in amu*A**2 and MHz."""
        self.inertia_tensor = -self.compute_planar_Inertia_tensor()
        np.fill_diagonal(self.inertia_tensor,
                         self.planar_inertia_tensor.diagonal())
        np.fill_diagonal(self.inertia_tensor,
                         self.inertia_tensor.diagonal().sum() -
                         self.inertia_tensor.diagonal())
        self.I_abc = np.linalg.eigvalsh(self.inertia_tensor)
        self.rot_const = conv / self.I_abc
        print('A = {} MHz, B = {} MHz, C = {} MHz'.format(*self.rot_const))

    def compute_ABC_coord(self):
        """Transform atomic coordinates into the principal axes system."""
        self.compute_rotational_constants()
        self.P_abc, self.rot_matrix = np.linalg.eigh(self.inertia_tensor)
        print('rot matrix\n', self.rot_matrix)
        self.P_abc = abs(self.P_abc)
        data = self.coordinates - self.com
        self.coord_ABC = np.dot(np.linalg.inv(self.rot_matrix),
                                data.transpose()).transpose()
        self.print_coords()

    @staticmethod
    def EFG_to_NQCC_conversion(Q_barn, Raw_EFG_matrix):
        """Convert the Raw EFG matrix in au^3 into the NQCC tensor in MHz.

        Parameters
        ----------
        Raw_EFG_matrix : 3D array
            Matrix from the electric field gradient from the calculation.
        Q_barn : float
            Quadrupolar moment in barn.
        """
        Q = Q_barn * 10**-28 * 100
        NQCC_MHz = np.array(Raw_EFG_matrix) * au**3 * Q * e**2
        return Q, NQCC_MHz

    @staticmethod
    def EFG_to_NQCC_conversion_Bailey(Q_barn, Raw_EFG_matrix, stripped_name):
        """Convert the Raw EFG matrix in au^3 into the NQCC tensor in MHz.
        Employs the conversion developed by Willian Bailey for clorine.
        http://nqcc.wcbailey.net/Chlorine.html

        Parameters
        ----------
        Raw_EFG_matrix : 3D array
            Matrix from the electric field gradient from the calculation.
        Q_barn : float
            Quadrupolar moment in barn.
        """
        Q = Q_barn * 10**-28 * 100
        NQCC_MHz = np.array(Raw_EFG_matrix) * BaileyConversion[stripped_name]
        return Q, NQCC_MHz

    @staticmethod
    def chi_calc(NQCC_MHz):
        """Calculate 3/2 * Chi_aa  and (Chi_bb - Chi_cc) for pickett."""
        Chi_aa = NQCC_MHz[0, 0] * 1.5
        Chi_bb_cc = (NQCC_MHz[1, 1] - NQCC_MHz[2, 2])
        Chi_bb_cc_4 = (NQCC_MHz[1, 1] - NQCC_MHz[2, 2]) / 4
        return Chi_aa, Chi_bb_cc, Chi_bb_cc_4

    def transform_chi_tensor(self, Bailey=False):
        """
        Transform EFG by rotating tensor into the PSA and converting to MHz.

        Parameters
        ----------
        Bailey : boolean, optional
            If Bailey = True, Q_eff method from William Bailey is used,
            Check for the correct calculation method of the nucleus.

        Yields
        ------
        Entry for each nucleus in the following order
        Q_barn, Q, EFG in old axis system, EFG in PSA, Chi tensor in old axis
        system, Chi tensor in PSA
        """
        if hasattr(self, 'rot_matrix'):
            pass
        else:
            self.compute_ABC_coord()

        if 'quad_nuc_name' not in self.results_dict:
            return

        if Bailey is False:
            for i, nuc_name in enumerate(self.results_dict['quad_nuc_name']):
                first_rot = np.dot(self.rot_matrix.transpose(),
                                   self.results_dict[nuc_name][1])
                rotated = np.dot(first_rot, self.rot_matrix)
                nuc_info = list(self.results_dict[nuc_name])
                nuc_info.append(rotated)
                Q, NQCC_nonPSA_MHz = self.EFG_to_NQCC_conversion(nuc_info[0],
                                                                 nuc_info[1])
                Q, NQCC_PSA_MHz = self.EFG_to_NQCC_conversion(nuc_info[0],
                                                              nuc_info[2])
                self.results_dict[nuc_name] = [nuc_info[0], Q, nuc_info[1],
                                               nuc_info[2], NQCC_nonPSA_MHz,
                                               NQCC_PSA_MHz]
        # Bailey conversion instead, check if the nucleus is on the list
        else:
            for i, nuc_name in enumerate(self.results_dict['quad_nuc_name']):
                first_rot = np.dot(self.rot_matrix.transpose(),
                                   self.results_dict[nuc_name][1])
                rotated = np.dot(first_rot, self.rot_matrix)
                nuc_info = list(self.results_dict[nuc_name])
                nuc_info.append(rotated)
                stripped_name = re.sub(r'\d+', '', nuc_name)
                if stripped_name in BaileyConversion.keys():
                    print(f'Used Baileys Qeff for nucleus {stripped_name}')
                    Q, NQCC_nonPSA_MHz = self.EFG_to_NQCC_conversion_Bailey(
                                                                nuc_info[0],
                                                                nuc_info[1],
                                                                stripped_name)
                    Q, NQCC_PSA_MHz = self.EFG_to_NQCC_conversion_Bailey(
                                                                nuc_info[0],
                                                                nuc_info[2],
                                                                stripped_name)
                    self.results_dict[nuc_name] = [nuc_info[0], Q, nuc_info[1],
                                                   nuc_info[2],
                                                   NQCC_nonPSA_MHz,
                                                   NQCC_PSA_MHz]
                else:
                    Q, NQCC_nonPSA_MHz = self.EFG_to_NQCC_conversion(
                                                                nuc_info[0],
                                                                nuc_info[1])
                    Q, NQCC_PSA_MHz = self.EFG_to_NQCC_conversion(
                                                                nuc_info[0],
                                                                nuc_info[2])
                    self.results_dict[nuc_name] = [nuc_info[0], Q, nuc_info[1],
                                                   nuc_info[2],
                                                   NQCC_nonPSA_MHz,
                                                   NQCC_PSA_MHz]

    def _m_transform_chi_tensor(self, job_num, Bailey=False):
        """
        Transform EFG by rotating tensor into the PSA and converting to MHz.

        Parameters
        ----------
        Bailey : boolean, optional
            If Bailey = True, Q_eff method from William Bailey is used,
            Check for the correct calculation method of the nucleus.

        job_num : int
            Job number in the Orca output file of the job to be treated.

        Yields
        ------
        Entry for each nucleus in the following order
        Q_barn, Q, EFG in old axis system, EFG in PSA, Chi tensor in old axis
        system, Chi tensor in PSA
        """
        if hasattr(self, 'rot_matrix'):
            pass
        else:
            self.compute_ABC_coord()

        if 'quad_nuc_name' not in self.results_dict[job_num]:
            return

        if Bailey is False:
            for i, nuc_name in enumerate(self.results_dict[job_num][
                                                        'quad_nuc_name']):
                first_rot = np.dot(self.rot_matrix.transpose(),
                                   self.results_dict[job_num][nuc_name][1])
                rotated = np.dot(first_rot, self.rot_matrix)
                nuc_info = list(self.results_dict[job_num][nuc_name])
                nuc_info.append(rotated)
                Q, NQCC_nonPSA_MHz = self.EFG_to_NQCC_conversion(nuc_info[0],
                                                                 nuc_info[1])
                Q, NQCC_PSA_MHz = self.EFG_to_NQCC_conversion(nuc_info[0],
                                                              nuc_info[2])
                self.results_dict[job_num][nuc_name] = [nuc_info[0], Q,
                                                        nuc_info[1],
                                                        nuc_info[2],
                                                        NQCC_nonPSA_MHz,
                                                        NQCC_PSA_MHz]
        # Bailey conversion instead, check if the nucleus is on the list
        else:
            for i, nuc_name in enumerate(self.results_dict[job_num][
                                        'quad_nuc_name']):
                first_rot = np.dot(self.rot_matrix.transpose(),
                                   self.results_dict[job_num][nuc_name][1])
                rotated = np.dot(first_rot, self.rot_matrix)
                nuc_info = list(self.results_dict[job_num][nuc_name])
                nuc_info.append(rotated)
                stripped_name = re.sub(r'\d+', '', nuc_name)
                if stripped_name in BaileyConversion.keys():
                    print(f'Used Baileys Qeff for nucleus {stripped_name}')
                    Q, NQCC_nonPSA_MHz = self.EFG_to_NQCC_conversion_Bailey(
                                                                nuc_info[0],
                                                                nuc_info[1],
                                                                stripped_name)
                    Q, NQCC_PSA_MHz = self.EFG_to_NQCC_conversion_Bailey(
                                                                nuc_info[0],
                                                                nuc_info[2],
                                                                stripped_name)
                    self.results_dict[job_num][nuc_name] = [nuc_info[0], Q,
                                                            nuc_info[1],
                                                            nuc_info[2],
                                                            NQCC_nonPSA_MHz,
                                                            NQCC_PSA_MHz]
                else:
                    Q, NQCC_nonPSA_MHz = self.EFG_to_NQCC_conversion(
                                                                nuc_info[0],
                                                                nuc_info[1])
                    Q, NQCC_PSA_MHz = self.EFG_to_NQCC_conversion(
                                                                nuc_info[0],
                                                                nuc_info[2])
                    self.results_dict[job_num][nuc_name] = [nuc_info[0], Q,
                                                            nuc_info[1],
                                                            nuc_info[2],
                                                            NQCC_nonPSA_MHz,
                                                            NQCC_PSA_MHz]

    def compute_mu_ABC(self):
        """"Rotate the dipole moment vector into the principal axis system."""
        if hasattr(self, 'rot_matrix'):
            pass
        else:
            self.compute_ABC_coord()
        self.results_dict['mu_abc'] = np.dot(self.results_dict['mu_xyz'],
                                             self.rot_matrix)

    def _categorize_file(self):
        """Categorize the input file.

        Distinguishes between xyz-files and orca output files from a
        single point calculation, geometry optimization.
        """
        if os.path.splitext(self.filename)[1] == '.xyz':
            self.file_type = '.xyz file'
        with open(self.filename) as file:
            for ind, line in enumerate(file, 1):
                if 'Version 3' in line:
                    program = 'orca3'
                if 'Version 4' in line:
                    program = 'orca4'
                if 'Version 5' in line:
                    program = 'orca5'

                if 'JOB NUMBER' in line:
                    multiple = True

                if '* Single Point Calculation *' in line:
                    if program == 'orca3':
                        self.file_type = 'Orca 3 SP'
                    if program == 'orca4':
                        self.file_type = 'Orca 4 SP'
                        try:
                            if multiple:
                                self.file_type = 'Orca 4 Multiple SP'
                        except UnboundLocalError:
                            pass
                        break
                    if program == 'orca5':
                        self.file_type = 'Orca 5 SP'
                        try:
                            if multiple:
                                self.file_type = 'Orca 5 Multiple SP'
                        except UnboundLocalError:
                            pass
                        break

                if 'GEOMETRY OPTIMIZATION' in line:
                    if program == 'orca3':
                        self.file_type = 'Orca 3 Geometry Opt'
                    if program == 'orca4':
                        self.file_type = 'Orca 4 Geometry Opt'
                        try:
                            if multiple:
                                self.file_type = 'Orca 4 Multiple Opt'
                        except UnboundLocalError:
                            pass
                        break
                    if program == 'orca5':
                        self.file_type = 'Orca 5 Geometry Opt'
                        try:
                            if multiple:
                                self.file_type = 'Orca 5 Multiple Opt'
                        except UnboundLocalError:
                            pass
                        break

                if 'STRFIT - General structure' in line:
                    self.file_type = 'Strfit output'
                    break
        if hasattr(self, 'file_type'):
            pass
        else:
            raise AttributeError('Geometry object has no attribute file_type')

    def _read_xyz_file(self):
        """Read the molecular coordinates in an .xyz file."""
        with open(self.filename) as orca_file:
            counter = 0
            self.coordinates = []
            self.names = []
            self.natoms = int(getline(orca_file.name, 1).strip())
            for idx, line in enumerate(orca_file, 1):
                coord_line = getline(orca_file.name, idx)
                if '\n' in coord_line:
                    counter += 1
                if counter < 3:
                    pass
                elif coord_line == '\n':
                    pass
                else:
                    coord_line = coord_line.split(' ')
                    coord_line = list(filter(None, coord_line))
                    name = coord_line[0]
                    self.names.append(name)
                    coord_line = [float(i) for i in coord_line[1:4]]
                    self.coordinates.append(coord_line)
        self.coordinates = np.array(self.coordinates)
        self.names = np.array(self.names)
        if self.natoms != self.coordinates.shape[0]:
            print(f'natoms according to file: {self.natoms}\n')
            print(f'ncoordinates read: {self.coordinates.shape[0]}')
            raise ValueError('Incorrect number of atoms where read')
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1
        self.print_coords()
        print('File successfully read!')

    def _read_Orca4_OPT_output(self):
        """Read the molecular coordinates in an ocra version 4 .out file.

        Reads files generated by orca in an optimization run.
        """
        with open(self.filename) as orca_file:
            self.results_dict = {}
            quad_nuc_name = []
            nuc_counter = 0
            for idx, line in enumerate(orca_file, 1):
                if line.startswith(
                        'Number of atoms                         ....'):
                    self.natoms = getline(orca_file.name, idx)
                    self.natoms = [float(i) for i in p.findall(self.natoms)][0]
                if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
                    self.coordinates = []
                    self.names = []
                    for i in np.arange(idx + 2, idx + 2 + int(self.natoms)):
                        coord_line = getline(orca_file.name, i)
                        name = coord_line.split(' ')[2]
                        self.names.append(name)
                        coord_line = [float(i) for i in p.findall(coord_line)]
                        self.coordinates.append(coord_line)

                # Read command line
                if '> !' in line:
                    self.command_line = getline(
                        orca_file.name, idx)[7:-2].strip()

                # read total energy
                if line.startswith('FINAL SINGLE POINT ENERGY'):
                    E_tot = getline(orca_file.name, idx)
                    E_tot = [float(i) for i in p.findall(E_tot)]
                    self.results_dict['E_tot'] = E_tot

                # check convergence
                if '***        THE OPTIMIZATION HAS CONVERGED     ***' in line:
                    self.results_dict['opt_convergence'] = True

                # read rotational constants
                if line.startswith('Rotational constants in MHz'):
                    rot_const_MHz = getline(orca_file.name, idx)
                    rot_const_MHz = [
                        float(i) for i in p.findall(rot_const_MHz)]
                    self.results_dict['rot_const_MHz'] = rot_const_MHz

                # read dipole moment along the rotational axes
                if line.startswith('Total Dipole Moment'):
                    mu_xyz = getline(orca_file.name, idx)
                    mu_xyz = [
                        float(i) for i in p.findall(mu_xyz)]
                    self.results_dict['mu_xyz'] = mu_xyz
                if line.startswith('x,y,z [Debye]:'):
                    mu_abc_debye = getline(orca_file.name, idx)
                    mu_abc_debye = [
                        float(i) for i in p.findall(mu_abc_debye)]
                    self.results_dict['mu_abc_debye'] = mu_abc_debye

                # read names of quadrupolar nuclei
                if line.startswith('   Nucleus:'):
                    nuc_name = getline(
                        orca_file.name, idx).split(':')[1].strip()
                    if nuc_name not in quad_nuc_name:
                        quad_nuc_name.append(nuc_name)
                        self.results_dict['quad_nuc_name'] = quad_nuc_name

                if len(quad_nuc_name) > nuc_counter:
                    # Read Q
                    if 'barn' in line:
                        Q_barn = getline(orca_file.name, idx)
                        Q_barn = [float(i) for i in p.findall(Q_barn)][-1]

                    # read EFG matrix
                    if 'Raw EFG matrix (all values in a.u.**-3):' in line:
                        Raw_EFG_matrix = np.zeros((3, 3))
                        for j in np.arange(3):
                            line = getline(orca_file.name, idx + j + 1)
                            Raw_EFG_matrix[j, :] = [
                                float(i) for i in p.findall(line)]
                        self.results_dict[quad_nuc_name[nuc_counter]] = (
                            Q_barn, Raw_EFG_matrix)
                        nuc_counter += 1

        self.coordinates = np.array(self.coordinates)
        self.names = np.array(self.names)
        if self.natoms != self.coordinates.shape[0]:
            print(f'natoms according to file: {self.natoms}\n')
            print(f'ncoordinates read: {self.coordinates.shape[0]}')
            raise ValueError('Incorrect number of atoms where read')
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1

        # check what was read
        if 'rot_const_MHz' not in self.results_dict.keys():
            warn('Found no rotational constants along the principal axis' +
                 'system in file')
        if 'mu_xyz' not in self.results_dict.keys():
            warn('Found no dipole moment components in the xyz axis sytem' +
                 'in file')
        if 'mu_abc_debye' not in self.results_dict.keys():
            warn('Found no dipole moment components in the abc axis sytem ' +
                 'in file')
        if 'E_tot' not in self.results_dict.keys():
            warn('Found no value for the total energy in file')
        if 'Q_barn' not in locals():
            warn('Found no value for Q in file')
        if 'Raw_EFG_matrix' not in locals():
            warn('Found no EFG matrix in file')
        if 'opt_convergence' not in self.results_dict.keys():
            self.results_dict['opt_convergence'] = False
            warn('Optimization not converged')

        # read xyz file instead of outputfile
        xyz_outputfile = '{}.xyz'.format(os.path.splitext(self.filename)[0])
        if os.path.isfile(xyz_outputfile):
            store_filename = self.filename
            self.filename = xyz_outputfile
            self._read_xyz_file()
            self.filename = store_filename
            print('Coordinates read for xyz file in same folder.')
        else:
            self.print_coords()
            print('File successfully read!')
            print('Coordinates read from Orca output file.')

    def _read_Orca5_OPT_output(self):
        """Read the molecular coordinates in an ocra version 5 .out file.

        Reads files generated by orca in an optimization run.
        """
        with open(self.filename) as orca_file:
            self.results_dict = {}
            quad_nuc_name = []
            nuc_counter = 0
            for idx, line in enumerate(orca_file, 1):
                if line.startswith(
                        'Number of atoms                         ....'):
                    self.natoms = getline(orca_file.name, idx)
                    self.natoms = [float(i) for i in p.findall(self.natoms)][0]
                if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
                    self.coordinates = []
                    self.names = []
                    for i in np.arange(idx + 2, idx + 2 + int(self.natoms)):
                        coord_line = getline(orca_file.name, i)
                        name = coord_line.split(' ')[2]
                        self.names.append(name)
                        coord_line = [float(i) for i in p.findall(coord_line)]
                        self.coordinates.append(coord_line)

                # Read command line
                if '> !' in line:
                    self.command_line = getline(
                        orca_file.name, idx)[7:-2].strip()

                # read total energy
                if line.startswith('FINAL SINGLE POINT ENERGY'):
                    E_tot = getline(orca_file.name, idx)
                    E_tot = [float(i) for i in p.findall(E_tot)]
                    self.results_dict['E_tot'] = E_tot

                # check convergence
                if '***        THE OPTIMIZATION HAS CONVERGED     ***' in line:
                    self.results_dict['opt_convergence'] = True

                # read rotational constants
                if line.startswith('Rotational constants in MHz'):
                    rot_const_MHz = getline(orca_file.name, idx)
                    rot_const_MHz = [
                        float(i) for i in p.findall(rot_const_MHz)]
                    self.results_dict['rot_const_MHz'] = rot_const_MHz

                # read dipole moment along the rotational axes
                if line.startswith('Total Dipole Moment'):
                    mu_xyz = getline(orca_file.name, idx)
                    mu_xyz = [
                        float(i) for i in p.findall(mu_xyz)]
                    self.results_dict['mu_xyz'] = mu_xyz
                if line.startswith('x,y,z [Debye]:'):
                    mu_abc_debye = getline(orca_file.name, idx)
                    mu_abc_debye = [
                        float(i) for i in p.findall(mu_abc_debye)]
                    self.results_dict['mu_abc_debye'] = mu_abc_debye

                # read names of quadrupolar nuclei
                if line.startswith('   Nucleus:'):
                    nuc_name = getline(
                        orca_file.name, idx).split(':')[1].strip()
                    if nuc_name not in quad_nuc_name:
                        quad_nuc_name.append(nuc_name)
                        self.results_dict['quad_nuc_name'] = quad_nuc_name

                if len(quad_nuc_name) > nuc_counter:
                    # Read Q
                    if 'barn' in line:
                        Q_barn = getline(orca_file.name, idx)
                        Q_barn = [float(i) for i in p.findall(Q_barn)][-1]

                    # read EFG matrix
                    if 'Raw EFG matrix (all values in a.u.**-3):' in line:
                        Raw_EFG_matrix = np.zeros((3, 3))
                        for j in np.arange(3):
                            line = getline(orca_file.name, idx + j + 2)
                            Raw_EFG_matrix[j, :] = [
                                float(i) for i in p.findall(line)]
                        self.results_dict[quad_nuc_name[nuc_counter]] = (
                            Q_barn, Raw_EFG_matrix)
                        nuc_counter += 1

        self.coordinates = np.array(self.coordinates)
        self.names = np.array(self.names)
        if self.natoms != self.coordinates.shape[0]:
            print(f'natoms according to file: {self.natoms}\n')
            print(f'ncoordinates read: {self.coordinates.shape[0]}')
            raise ValueError('Incorrect number of atoms where read')
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1

        # check what was read
        if 'rot_const_MHz' not in self.results_dict.keys():
            warn('Found no rotational constants along the principal axis' +
                 'system in file')
        if 'mu_xyz' not in self.results_dict.keys():
            warn('Found no dipole moment components in the xyz axis sytem' +
                 'in file')
        if 'mu_abc_debye' not in self.results_dict.keys():
            warn('Found no dipole moment components in the abc axis sytem ' +
                 'in file')
        if 'E_tot' not in self.results_dict.keys():
            warn('Found no value for the total energy in file')
        if 'Q_barn' not in locals():
            warn('Found no value for Q in file')
        if 'Raw_EFG_matrix' not in locals():
            warn('Found no EFG matrix in file')
        if 'opt_convergence' not in self.results_dict.keys():
            self.results_dict['opt_convergence'] = False
            warn('Optimization not converged')

        # read xyz file instead of outputfile
        xyz_outputfile = '{}.xyz'.format(os.path.splitext(self.filename)[0])
        if os.path.isfile(xyz_outputfile):
            store_filename = self.filename
            self.filename = xyz_outputfile
            self._read_xyz_file()
            self.filename = store_filename
            print('Coordinates read for xyz file in same folder.')
        else:
            self.print_coords()
            print('File successfully read!')
            print('Coordinates read from Orca output file.')

    def _read_Orca4_SP_output(self):
        """Read the molecular coordinates in an ocra version 4 .out file.

        Reads files generated by orca in an Single Point calculation.
        """
        self.coordinates = []
        self.names = []
        with open(self.filename) as orca_file:
            self.results_dict = {}
            quad_nuc_name = []
            nuc_counter = 0
            end = []
            input_end = 10000
            for idx, line in enumerate(orca_file, 1):
                if '* xyz' in line:
                    start = idx
                if '****END OF INPUT****' in line:
                    input_end = idx
                if ('*' in line and idx <= input_end):
                    end.append(idx)
                # read rotational constants
                if line.startswith('Rotational constants in MHz'):
                    rot_const_MHz = getline(orca_file.name, idx)
                    rot_const_MHz = [
                        float(i) for i in p.findall(rot_const_MHz)]
                    self.results_dict['rot_const_MHz'] = rot_const_MHz

                # Read command line
                if '> !' in line:
                    self.command_line = getline(
                        orca_file.name, idx)[7:-2].strip()

                # read dipole moment along the rotational axes
                if line.startswith('Total Dipole Moment'):
                    mu_xyz = getline(orca_file.name, idx)
                    mu_xyz = [
                        float(i) for i in p.findall(mu_xyz)]
                    self.results_dict['mu_xyz'] = mu_xyz
                if line.startswith('x,y,z [Debye]:'):
                    mu_abc_debye = getline(orca_file.name, idx)
                    mu_abc_debye = [
                        float(i) for i in p.findall(mu_abc_debye)]
                    self.results_dict['mu_abc_debye'] = mu_abc_debye

                # read total energy
                if line.startswith('FINAL SINGLE POINT ENERGY'):
                    E_tot = getline(orca_file.name, idx)
                    E_tot = [float(i) for i in p.findall(E_tot)]
                    self.results_dict['E_tot'] = E_tot

                # read names of quadrupolar nuclei
                if line.startswith('   Nucleus:'):
                    nuc_name = getline(
                        orca_file.name, idx).split(':')[1].strip()
                    quad_nuc_name.append(nuc_name)
                    self.results_dict['quad_nuc_name'] = quad_nuc_name

                # Read Q
                if 'barn' in line:
                    Q_barn = getline(orca_file.name, idx)
                    Q_barn = [float(i) for i in p.findall(Q_barn)][-1]

                # read EFG matrix
                if ' Raw EFG matrix (all values in a.u.**-3):' in line:
                    Raw_EFG_matrix = np.zeros((3, 3))
                    for j in np.arange(3):
                        line = getline(orca_file.name, idx + j + 1)
                        Raw_EFG_matrix[j, :] = [
                            float(i) for i in p.findall(line)]
                    self.results_dict[quad_nuc_name[nuc_counter]] = (
                        Q_barn, Raw_EFG_matrix)
                    nuc_counter += 1
            # read coordinates
            end = end[end.index(start) + 1]
            for i in np.arange(start + 1, end):
                coord_line = getline(orca_file.name, i)
                coord_line = coord_line.split(' ')
                coord_line = list(filter(None, coord_line))
                if len(coord_line) < 4:
                    continue
                name = coord_line[2]
                self.names.append(name)
                coord_line = [float(i) for i in coord_line[3:6]]
                self.coordinates.append(coord_line)
        self.coordinates = np.array(self.coordinates)
        self.names = np.array(self.names)
        self.natoms = self.coordinates.shape[0]
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1
        self.print_coords()
        # check what was read
        if 'rot_const_MHz' not in self.results_dict.keys():
            warn('Found no rotational constants along the principal axis' +
                 'system in file')
        if 'mu_xyz' not in self.results_dict.keys():
            warn('Found no dipole moment components in the xyz axis sytem' +
                 'in file')
        if 'mu_abc_debye' not in self.results_dict.keys():
            warn('Found no dipole moment components in the abc axis sytem ' +
                 'in file')
        if 'E_tot' not in self.results_dict.keys():
            warn('Found no value for the total energy in file')
        if 'Q_barn' not in locals():
            warn('Found no value for Q in file')
        if 'Raw_EFG_matrix' not in locals():
            warn('Found no EFG matrix in file')
        print('SP.out file successfully read!')

    def _read_Orca5_SP_output(self):
        """Read the molecular coordinates in an ocra version 5 .out file.

        Reads files generated by orca in an Single Point calculation.
        """
        self.coordinates = []
        self.names = []
        with open(self.filename) as orca_file:
            self.results_dict = {}
            quad_nuc_name = []
            nuc_counter = 0
            end = []
            input_end = 10000
            for idx, line in enumerate(orca_file, 1):
                if '* xyz' in line:
                    start = idx
                if '****END OF INPUT****' in line:
                    input_end = idx
                if ('*' in line and idx <= input_end):
                    end.append(idx)
                # read rotational constants
                if line.startswith('Rotational constants in MHz'):
                    rot_const_MHz = getline(orca_file.name, idx)
                    rot_const_MHz = [
                        float(i) for i in p.findall(rot_const_MHz)]
                    self.results_dict['rot_const_MHz'] = rot_const_MHz

                # Read command line
                if '> !' in line:
                    self.command_line = getline(
                        orca_file.name, idx)[7:-2].strip()

                # read dipole moment along the rotational axes
                if line.startswith('Total Dipole Moment'):
                    mu_xyz = getline(orca_file.name, idx)
                    mu_xyz = [
                        float(i) for i in p.findall(mu_xyz)]
                    self.results_dict['mu_xyz'] = mu_xyz
                if line.startswith('x,y,z [Debye]:'):
                    mu_abc_debye = getline(orca_file.name, idx)
                    mu_abc_debye = [
                        float(i) for i in p.findall(mu_abc_debye)]
                    self.results_dict['mu_abc_debye'] = mu_abc_debye

                # read total energy
                if line.startswith('FINAL SINGLE POINT ENERGY'):
                    E_tot = getline(orca_file.name, idx)
                    E_tot = [float(i) for i in p.findall(E_tot)]
                    self.results_dict['E_tot'] = E_tot

                # read names of quadrupolar nuclei
                if line.startswith('   Nucleus:'):
                    nuc_name = getline(
                        orca_file.name, idx).split(':')[1].strip()
                    quad_nuc_name.append(nuc_name)
                    self.results_dict['quad_nuc_name'] = quad_nuc_name

                # Read Q
                if 'barn' in line:
                    Q_barn = getline(orca_file.name, idx)
                    Q_barn = [float(i) for i in p.findall(Q_barn)][-1]

                # read EFG matrix
                if ' Raw EFG matrix (all values in a.u.**-3):' in line:
                    Raw_EFG_matrix = np.zeros((3, 3))
                    for j in np.arange(3):
                        line = getline(orca_file.name, idx + j + 2)
                        Raw_EFG_matrix[j, :] = [
                            float(i) for i in p.findall(line)]
                    self.results_dict[quad_nuc_name[nuc_counter]] = (
                        Q_barn, Raw_EFG_matrix)
                    nuc_counter += 1
            # read coordinates
            end = end[end.index(start) + 1]
            for i in np.arange(start + 1, end):
                coord_line = getline(orca_file.name, i)
                coord_line = coord_line.split(' ')
                coord_line = list(filter(None, coord_line))
                if len(coord_line) < 4:
                    continue
                name = coord_line[2]
                self.names.append(name)
                coord_line = [float(i) for i in coord_line[3:6]]
                self.coordinates.append(coord_line)
        self.coordinates = np.array(self.coordinates)
        self.names = np.array(self.names)
        self.natoms = self.coordinates.shape[0]
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1
        self.print_coords()
        # check what was read
        if 'rot_const_MHz' not in self.results_dict.keys():
            warn('Found no rotational constants along the principal axis' +
                 'system in file')
        if 'mu_xyz' not in self.results_dict.keys():
            warn('Found no dipole moment components in the xyz axis sytem' +
                 'in file')
        if 'mu_abc_debye' not in self.results_dict.keys():
            warn('Found no dipole moment components in the abc axis sytem ' +
                 'in file')
        if 'E_tot' not in self.results_dict.keys():
            warn('Found no value for the total energy in file')
        if 'Q_barn' not in locals():
            warn('Found no value for Q in file')
        if 'Raw_EFG_matrix' not in locals():
            warn('Found no EFG matrix in file')
        print('SP.out file successfully read!')

    def _read_multiple_Orca5_SP_output(self, job_num=False):
        """
        Read first molecular coordinates in job file with multiple jobs.

        Reads files generated by orca in file with multiple jobs.

        Parameters
        ----------
        job_num : bool, int
            Job number which is chosen for the read coordinates. Default is
            job with the most atoms.
        """
        self.coordinates = []
        self.names = []
        with open(self.filename) as orca_file:
            self.results_dict = defaultdict(dict)
            quad_nuc_name = []
            nuc_counter = 0
            start = []
            end = []
            input_end = 10000
            for idx, line in enumerate(orca_file, 1):
                if '* xyz' in line:
                    start.append(idx)
                if '****END OF INPUT****' in line:
                    input_end = idx
                if ('*' in line and idx < input_end):
                    end.append(idx)
                if 'JOBS TO BE PROCESSED THIS RUN $' in line:
                    n_jobs = getline(orca_file.name, idx)
                    n_jobs = [
                        int(i) for i in p.findall(n_jobs)][0]
                    self.n_jobs = n_jobs
                if 'JOB NUMBER' in line:
                    job = getline(orca_file.name, idx)
                    job = [
                        int(i) for i in p.findall(job)][0]
                    nuc_counter = 0
                    quad_nuc_name = []

                # read rotational constants
                if line.startswith('Rotational constants in MHz'):
                    rot_const_MHz = getline(orca_file.name, idx)
                    rot_const_MHz = [
                        float(i) for i in p.findall(rot_const_MHz)]
                    self.results_dict[job]['rot_const_MHz'] = rot_const_MHz

                # Read command line
                if '> !' in line:
                    self.command_line = getline(
                        orca_file.name, idx)[7:-2].strip()

                # read dipole moment along the rotational axes
                if line.startswith('Total Dipole Moment'):
                    mu_xyz = getline(orca_file.name, idx)
                    mu_xyz = [
                        float(i) for i in p.findall(mu_xyz)]
                    self.results_dict[job]['mu_xyz'] = mu_xyz
                if line.startswith('x,y,z [Debye]:'):
                    mu_abc_debye = getline(orca_file.name, idx)
                    mu_abc_debye = [
                        float(i) for i in p.findall(mu_abc_debye)]
                    self.results_dict[job]['mu_abc_debye'] = mu_abc_debye

                # read total energy
                if line.startswith('FINAL SINGLE POINT ENERGY'):
                    E_tot = getline(orca_file.name, idx)
                    E_tot = [float(i) for i in p.findall(E_tot)]
                    self.results_dict[job]['E_tot'] = E_tot

                # read names of quadrupolar nuclei
                if line.startswith('   Nucleus:'):
                    nuc_name = getline(
                        orca_file.name, idx).split(':')[1].strip()
                    quad_nuc_name.append(nuc_name)
                    self.results_dict[job]['quad_nuc_name'] = quad_nuc_name

                # Read Q
                if 'barn' in line:
                    Q_barn = getline(orca_file.name, idx)
                    Q_barn = [float(i) for i in p.findall(Q_barn)][-1]

                # read EFG matrix
                if ' Raw EFG matrix (all values in a.u.**-3):' in line:
                    Raw_EFG_matrix = np.zeros((3, 3))
                    for j in np.arange(3):
                        line = getline(orca_file.name, idx + j + 2)
                        Raw_EFG_matrix[j, :] = [
                            float(i) for i in p.findall(line)]
                    self.results_dict[job][quad_nuc_name[nuc_counter]] = (
                        Q_barn, Raw_EFG_matrix)
                    nuc_counter += 1

                # read job name
                if 'from job' in line:
                    job_name = getline(orca_file.name, idx)
                    job_name, job = re.split(r'from job', job_name)
                    job = int([float(i) for i in p.findall(job)][0])
                    job_name = re.split(r'Energy for', job_name)[1]
                    job_name = job_name.strip()
                    self.results_dict[job]['job_name'] = job_name

            # read coordinates

            end = [idx for idx in end if idx > start[0] and idx not in start]
            self._coord_end = end
            self._coord_start = start
            if job_num is True:
                job_num = input(f"Enter job number between 1 and {n_jobs} " +
                                "for loading coordinates ")
                job_num = int(float(job_num))
            elif type(job_num) is int:
                if job_num not in self.results_dict.keys():
                    job_num = input("Enter job integer number between 1 and " +
                                    f"{n_jobs} for loading coordinates ")
                    job_num = int(job_num)
            elif type(job_num) is float:
                job_num = int(job_num)
                if job_num not in self.results_dict.keys():
                    job_num = input("Enter job number between 1 and " +
                                    f"{n_jobs} for loading coordinates ")
                    job_num = int(job_num)
            else:
                natoms = [end[idx] - sta for idx, sta in enumerate(start)]
                job_num = np.argmax(natoms) + 1
            self.job_name = self.results_dict[job_num]['job_name']
            end = end[job_num - 1]
            start = start[job_num - 1]
            for i in np.arange(start + 1, end):
                coord_line = getline(orca_file.name, i)
                coord_line = coord_line.split(' ')
                coord_line = list(filter(None, coord_line))
                if len(coord_line) < 4:
                    continue
                name = coord_line[2]
                self.names.append(name)
                coord_line = [float(i) for i in coord_line[3:6]]
                self.coordinates.append(coord_line)

        self.coordinates = np.array(self.coordinates)
        self.names = np.array(self.names)
        self.natoms = self.coordinates.shape[0]
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1
        self.print_coords()
        # check what was read
        for job in np.arange(1, n_jobs + 1):
            if 'rot_const_MHz' not in self.results_dict[job].keys():
                warn('Found no rotational constants along the principal axis' +
                     f'system for {job} in file')
            if 'mu_xyz' not in self.results_dict[job].keys():
                warn('Found no dipole moment components in the xyz axis ' +
                     f'system for {job} in file')
            if 'mu_abc_debye' not in self.results_dict[job].keys():
                warn('Found no dipole moment components in the abc axis ' +
                     'system for {job} in file')
            if 'E_tot' not in self.results_dict[job].keys():
                warn(f'Found no value for the total energy in file for {job}')
            if 'job_name' not in self.results_dict[job].keys():
                warn(f'Found no job name in file for {job}')
            if 'Q_barn' not in locals():
                warn('Found no value for Q in file')
            if 'Raw_EFG_matrix' not in locals():
                warn('Found no EFG matrix in file')
        print(f'{n_jobs} jobs successfully read! Geometry from job ' +
              f'{job_num}: {self.results_dict[job_num]["job_name"]}')

    def _read_strfit_output(self):
        with open(self.filename) as file:
            self.coordinates = []
            self.coord_uncertainty = []
            self.atnum = []
            self.mass = []
            self.results_dict = {'convergence': True}
            self.natoms = int(extract_numbers(getline(file.name, 14))[0])
            for idx, line in enumerate(file, 1):
                if '      Chi-squared' in line:
                    self.results_dict['Chi-squared'] = extract_numbers(
                                                    getline(file.name, idx))[0]
                if 'Chisq/Ndegf' in line:
                    deviation_idx = idx
                if 'Deviation of fit' in line:
                    try:
                        self.results_dict['Deviation'] = extract_numbers(
                                                    getline(file.name,
                                                            deviation_idx))[0]
                    except NameError:
                        self.results_dict['Deviation'] = extract_numbers(
                                                    getline(file.name, idx))[0]
                if 'Final principal coordinates of parent:' in line:
                    for i in np.arange(idx + 4, idx + 4 + self.natoms):
                        line = extract_numbers(getline(file.name, i))
                        self.atnum.append(line[0])
                        self.coordinates.append(line[1:-1])
                        self.mass.append(line[-1])
                if 'Principal coordinates and estimated uncertainties' in line:
                    for i in np.arange(idx + 4, idx + 4 + self.natoms):
                        self.coord_uncertainty.append(
                                extract_numbers(getline(file.name, i))[2::2])
                if 'TOTAL NUMBER OF PARAMETERS:' in line:
                    self.nparams = extract_numbers(getline(file.name, idx))[0]
                if 'TOTAL NUMBER OF STRUCTURAL PARAMETERS:' in line:
                    self.nparams = extract_numbers(getline(file.name, idx))[0]
                if 'FINAL RESULTS OF LEAST SQUARES FIT:' in line:
                    for idx2 in np.arange(idx + 2, idx + 2 + self.nparams):
                        line = getline(file.name, idx2)
                        paramname = line[:20].strip()
                        self.results_dict[paramname] = extract_numbers(
                                                                    line)[-2:]
                if 'WARNING: poor convergence!' in line:
                    self.results_dict['convergence'] = False
                    warn('Poor convergence!')

            self.names = [nearest_key(m) for m in self.mass]
            self.coordinates = np.array(self.coordinates)
            self.names = np.array(self.names)
            self.mass = np.array(self.mass)
            self.atnum = np.array(self.atnum)
            self.print_coords()
            if 'Chi-squared' not in self.results_dict.keys():
                self.results_dict['convergence'] = False
                warn('Fit not successful!')
                return
            print('strfit.out file successfully read!')

    def reorder_coordinates(self, order):
        """Reorder the atoms.

        Parameters
        ----------
        order : lst
            List of integers for the rearanging the coordinates. List the
            current atom number in the desired order. Default is None.
        """
        if len(order) != int(self.natoms):
            raise ValueError(f'Length of order list {len(order)} does not' +
                             f' match number of atoms {int(self.natoms)} ')
        coords_reordered = np.zeros(self.coordinates.shape)
        names_reordered = list(np.arange(self.names.size))
        mass_reordered = np.zeros(self.mass.shape)
        for idx in np.arange(int(self.natoms)):
            names_reordered[idx] = self.names[order[idx]-1]
            coords_reordered[idx] = self.coordinates[order[idx]-1]
            mass_reordered[idx] = self.mass[order[idx]-1]
        self.names = names_reordered
        self.mass = mass_reordered
        self.coordinates = coords_reordered

    def add_atoms(self, atoms=None, coordinates=None):
        """
        Add atoms and coordinates.
        This function's primary use is to add dummy atoms for various further
        calculations.

        Parameters
        ----------
        atoms : array like of str, optional
            String of list of stings with atom labels. x is a dummy atom with
            mass 0 u. The default is None.
        coordinates : array like, optional
            Array with coordinates of shape (x, 3). x needs to be the same,
            length as the number of atoms given. The default is None.

        Returns
        -------
        None.
        """
        # Prompt for adding atoms manually one by one
        if atoms is None:
            atoms = []
            coordinates = []
            additions = True
            counter = 0
            while additions:
                add_atoms = input("Enter atom label to be added:")
                atoms.append(str(add_atoms))
                x = input('Coordinates of the additional atom: \n X:')
                y = input('Y:')
                z = input('Z:')
                coordinates.append([float(x), float(y), float(z)])
                inp = input('Enter additional atom? [y/N]')
                if np.logical_and('Y' != inp, 'y' != inp):
                    additions = False
                    break
                else:
                    counter = +1
                    if counter > 1000:
                        print('Emergency stop')
                        additions = False
                        break
        # Check if atom labels are allowed
        atoms = np.array(atoms)
        try:
            [masses[key] for key in atoms]
        except KeyError:
            print('Atom label unknown. Please correct labeling. Dummy atoms' +
                  'should be labeled x')
            atoms = []
            coordinates = []
            add_atoms = input("Enter atom label to be added:")
            atoms.append(str(add_atoms))
            x = input('Coordinates of the additional atom: \n X:')
            y = input('Y:')
            z = input('Z:')
            coordinates.append([float(x), float(y), float(z)])
            atoms = np.array(atoms)
        # Check if the number of coordinates matches the number of atom labels
        coordinates = np.array(coordinates)
        coordinates = coordinates.reshape(int(coordinates.size / 3), 3)
        coordinates = np.asarray(coordinates, dtype=float)
        if atoms.size == coordinates.shape[0]:
            pass
        else:
            raise ValueError('Number of atom names does not match number' +
                             ' of coordinates.')
        # Adding inputs to Geometry
        self.coordinates = np.append(self.coordinates, coordinates)
        self.coordinates = self.coordinates.reshape(
                            int(self.coordinates.size / 3), 3)
        self.names = np.append(self.names, atoms)
        self.natoms = self.natoms + atoms.size
        self.mass = np.array([masses[key] for key in self.names])
        self.atnum = np.arange(self.natoms, dtype=int) + 1
        self.print_coords()

    def remove_atoms(self, atnum):
        """
        Remove atoms from geometry.

        Parameters
        ----------
        atnum : array like
            List with atom numbers that should be kept in Geometry. Does NOT
            reorder atoms.

        Returns
        -------
        None.
        """
        # Check if input is correct
        atnum = np.asarray(atnum, dtype=int)
        atnum = np.unique(atnum)
        atnum = atnum[atnum > 0]
        if atnum[-1] > self.atnum[-1]:
            raise ValueError('Given atom number label higher than the ones ' +
                             'present.\n' +
                             f'Highest atom label given: {atnum[-1]} \n' +
                             f'Highest atom label present: {self.atnum[-1]}')
        if atnum.size > self.natoms:
            raise ValueError('More atom numbers given than atoms present \n' +
                             f'Number of atoms to keep: {atnum.size} \n' +
                             f'Number of atoms in geometry: {self.natoms}')
        self.names = np.array([self.names[i-1] for i in atnum])
        self.coordinates = np.array([self.coordinates[i-1] for i in atnum])
        self.mass = np.array([masses[key] for key in self.names])
        self.natoms = atnum.size
        self.atnum = np.arange(self.natoms, dtype=int) + 1
        self.print_coords()

    def _create_Orca_input(self, command_line, outputfile=None, comment='',
                           additional_commands='', mult='0 1'):
        """Create an input file with coordinates in the principal axes system.

        Parameters
        ----------
        command_line : str
            String of the orca command line for the calculation input file.
            View orca manual for details
        https://cec.mpg.de/fileadmin/media/Forschung/ORCA/orca_manual_4_0_1.pdf
        outputfile : str, optional
            Name of the output file. If none is given, the filename given to
            the class will be used with an .inp as a file name.
        comment : str, optional
            Optional comment for in the section of the input file.
        additional_commands : str, optional
            Additional commands to be placed after the coordinates regarding
            things like parallelisation.
        mult : str, optional
            Charge and of the compound, default is '0 1'

        Yields
        ------
        Text file which can be used as an orca input file with the molecular
        coordinates in the principal axes system.
        """

        warn('This function is depricated, use create_templated_orca_input' +
             ' instead')
        if outputfile is None:
            outputfile = '{}.inp'.format(os.path.splitext(self.filename)[0])

        self.compute_ABC_coord()
        with open(outputfile, 'w', encoding='utf-8') as file:
            file.write('# orca inputfile created in python\n')
            file.write('# coordinates in the principal axes system\n')
            file.write('# ' + comment.rstrip() + '\n')
            file.write(command_line.rstrip() + '\n\n')
            file.write('* xyz ' + mult + '\n')
            for idx, name in enumerate(self.names):
                name = name.rjust(4, ' ')
                x = str(self.coord_ABC[idx, 0]).rjust(24, ' ')
                y = str(self.coord_ABC[idx, 1]).rjust(24, ' ')
                z = str(self.coord_ABC[idx, 2]).rjust(24, ' ')
                file.write(name + x + y + z + '\n')
            file.write('*\n\n')
            file.write(additional_commands)
        print('Input file created successfully at: ', outputfile)

    def create_templated_orca_input(self, template, outputfile=None,
                                    command_line=None, comment=None,
                                    order=None):
        """
        Create ORCA input file based on input template, by incerting cartesian
        geometry.

        Parameters
        ----------
        template : str
            Template input file for templating. It has to be a formated ORCA
            input file utilizing cartesian coordinate. For details reference
            the ORCA manual.
        https://cec.mpg.de/fileadmin/media/Forschung/ORCA/orca_manual_4_0_1.pdf
        outputfile : str, optional
            Name of the output file. If none is given, the filename given to
            the class will be used with an .inp extension as a file name.
        command_line : str, optional
            Replacement for the command line in the ORCA input file, useful
            if operation is to be looped and the only change in template is
            the method. The default is None.
        comment : str, optional
            Comment to be added at the top of the ORCA input. This comment
            will not be parsed by ORCA. The default is None.
        order : lst, optional
            List of integers for the rearanging the coordinates. List the
            current atom number in the desired order. Default is None.

        Yields
        -------
        Creates an ORCA input file at the given location. All commands are
        taken from the template given with the exception of the cartesian
        coordinates, which are replaces with the coordinates read through
        Geometry.

        """
        if outputfile is None:
            outputfile = '{}.inp'.format(os.path.splitext(self.filename)[0])
        with open(template) as file:
            data = file.readlines()
        data = [line.rstrip() + '\n' for line in data]

        # Replace command line in template if desired (for looping etc)
        if command_line is not None:
            commandIDX = [i for i, string in enumerate(data) if '!' in string]
            for i in commandIDX:
                data[i] = command_line.rstrip() + '\n'

        if comment is not None:
            comment = ['# ' + comment.rstrip() + '\n']
            data = comment + data

        # Find index for cartesian data to be replaced. Also checks if valid
        # Orca input file.
        FalseFile = False
        try:
            for idx, line in enumerate(data):
                if '* xyz' in line:
                    startIDX = idx
            endIDX = data.index('*\n')
        except ValueError:
            FalseFile = True
        if FalseFile:
            raise ValueError('Template file not conforming to Orca input ' +
                             'standarts. Please check template file.')

        # Prepare coordinates of file
        if order is not None:
            self.reorder_coordinates(order)

        self.compute_ABC_coord()

        coord_data = []
        for idx, name in enumerate(self.names):
            name = name.rjust(4, ' ')
            x = str(self.coord_ABC[idx, 0]).rjust(24, ' ')
            y = str(self.coord_ABC[idx, 1]).rjust(24, ' ')
            z = str(self.coord_ABC[idx, 2]).rjust(24, ' ')
            coord_data.append(name + x + y + z + '\n')

        data = data[:startIDX + 1] + coord_data + data[endIDX:]

        with open(outputfile, 'w', encoding='utf-8') as file:
            file.writelines(data)
        print('ORCA input file created successfully at: ', outputfile)

    def create_orca_SP(self, additional_commands=None, parallelisation=False):
        """
        Create and input file for a SP calcualtion based on the opt input.

        Parameters
        ----------
        additional_commands : str
        Additional commands to add to the command line. Default is None.

        parallelisation : int
        Number of tasks for parallelisation. If True number of parallelisations
        in file is used. If False there won't be parallelisaton.
        Default is False.

        Yields
        ------
        Writes an inputfile for an Orca SP calculation in the filefolder. The
        folder also needs to contain and inputfile for the optimization.
        The geometry is in the principal axis system other commands such as
        """
        outputfile = '{}-SP.inp'.format(os.path.splitext(self.filename)[0])
        inputfile = '{}.inp'.format(os.path.splitext(self.filename)[0])
        if os.path.isfile(inputfile):
            pass
        else:
            print("Opt inputfile doesn't exists")
            return
        shutil.copy2(inputfile, outputfile)
        start = []
        self.compute_ABC_coord()
        with open(outputfile, 'r') as file:
            data = file.readlines()
        for idx, line in enumerate(data):
            if line.startswith('!'):
                newline = line.replace(' VERYTIGHTOPT', '')
                newline = newline.replace(' VERYTIGHTSCF', '')
                newline = newline.replace(' Opt', '')
                data[idx] = newline.replace('\n', ' SP\n')
            if line.startswith('*'):
                start.append(idx + 1)
            if line.startswith('%pal nprocs'):
                para_idx = idx
        for idx, name in enumerate(self.names):
            name = name.rjust(4, ' ')
            x = str(self.coord_ABC[idx, 0]).rjust(24, ' ')
            y = str(self.coord_ABC[idx, 1]).rjust(24, ' ')
            z = str(self.coord_ABC[idx, 2]).rjust(24, ' ')
            data[start[0] + idx] = name + x + y + z + '\n'
        if additional_commands is not None:
            data.append(additional_commands)
        if parallelisation is False:
            try:
                data[para_idx] = ''
            except UnboundLocalError:
                pass
        elif parallelisation is True:
            pass
        else:
            try:
                data[para_idx] = ''
            except UnboundLocalError:
                pass
            data.append('%pal nprocs {} end\n'.format(parallelisation))
        with open(outputfile, 'w', encoding='utf-8') as file:
            file. writelines(data)
        print('SP input file created successfully at: ', outputfile)

    def create_xyz_file(self, outputfile=None, order=None, comment=None):
        """Create an xyz file with coordinates in the principal axes system.

        Parameters
        ----------
        outputfile : str, optional
            Name of the xyz file. If none is given, the filename given to
            the class will be used with an .xyz as a file name.
        order : lst, optional
            List of integers for the rearanging the coordinates. List the
            current atom number in the desired order. Default is None.
        comment : str, optional
            Optional comment for in the section of the input file. Default is
            None.

        Yields
        ------
        Text file of coordinates in xyz file format.
        """
        if outputfile is None:
            outputfile = '{}.xyz'.format(os.path.splitext(self.filename)[0])

        if comment is None:
            comment = os.path.splitext(self.filename)[0]
        if order is not None:
            self.reorder_coordinates(order)
        self.compute_ABC_coord()
        with open(outputfile, 'w', encoding='utf-8') as file:
            file.write('{}\n'.format(int(self.natoms)))
            file.write('PAS coordinates' + comment + '\n')
            for idx, name in enumerate(self.names):
                name = self.names[idx].rjust(4, ' ')
                x = str(self.coord_ABC[idx, 0]).rjust(24, ' ')
                y = str(self.coord_ABC[idx, 1]).rjust(24, ' ')
                z = str(self.coord_ABC[idx, 2]).rjust(24, ' ')
                file.write(name + x + y + z + '\n')
        print('.xyz file created successfully at: ', outputfile)

    def create_gjf_file(self, outputfile=None, order=None, comment=None):
        """Create a gjf file with coordinates in the principal axes system.

        Parameters
        ----------
        output_file : str, optional
            Name of the .gjf file. If none is given, the filename given to
            the class will be used with an .gjf as a file name.
        order : lst, optional
            List of integers for the rearanging the coordinates. List the
            current atom number in the desired order. Default is None.
        comment : str, optional
            Optional comment for in the section of the input file.

        Yields
        ------
        Text file of coordinates in gjf file format.
        """
        if outputfile is None:
            outputfile = '{}.gjf'.format(os.path.splitext(self.filename)[0])

        if comment is None:
            comment = os.path.splitext(self.filename)[0]

        if order is not None:
            self.reorder_coordinates(order)

        self.compute_ABC_coord()
        with open(outputfile, 'w', encoding='utf-8') as file:
            file.write('{}\n\n'.format(comment))
            file.write('Reformat for GaussView\n\n')
            file.write('0 1\n')
            for idx, name in enumerate(self.names):
                name = self.names[idx].rjust(4, ' ')
                x = str(self.coord_ABC[idx, 0]).rjust(24, ' ')
                y = str(self.coord_ABC[idx, 1]).rjust(24, ' ')
                z = str(self.coord_ABC[idx, 2]).rjust(24, ' ')
                file.write(name + x + y + z + '\n')
        print('.gjf file created successfully at: ', outputfile)

    def _check_connection_matrix(self, connection):
        """Check if connection matrix for interal coordinates is valid."""
        connection = np.array(connection)
        # Check the size of the connection matrix
        if connection.shape != self.coordinates.shape:
            raise ValueError(f"Connection matrix of shape {connection.shape}" +
                             " doesn't match coordinates of" +
                             f" shape{self.coordinates.shape}")
        # Check if zeros are set correctly
        mask = connection[:3, :3] == 0
        mask2 = np.isnan(connection[:3, :3])
        boolean_template = np.array([[True, True, True],
                                    [False, True, True],
                                    [False, False, True]])
        comparison = np.logical_and(boolean_template != mask,
                                    boolean_template != mask2)

        if np.any(comparison):
            raise ValueError('Use 0 or np.nan to signify unused bond, angle' +
                             ' and dihedral angle indices')
        # Check if atom numbers are valid
        if connection[1, 0] <= 0:
            raise ValueError('0 is not a valid atom number')
        if connection[2, 0] <= 0:
            raise ValueError('0 is not a valid atom number')
        if connection[2, 1] <= 0:
            raise ValueError('0 is not a valid atom number')
        if np.any(connection[3:] <= 0):
            raise ValueError('Atom numbers need to be positive integers')

        # Check the vality of the elements
        for idx, con_row in enumerate(connection):
            if np.any(con_row >= idx + 1):
                raise ValueError('Connection matrix elements wrongly defined' +
                                 f' for atom {idx+1}')
            if idx > 1:
                if np.unique(con_row).size != 3:
                    raise ValueError('Matrix requires three unique atom' +
                                     ' numbers')

        # add valid connection matrix as object
        self.connection = connection.astype(int)
        self.connection[:3, :3][boolean_template] = 0

    @staticmethod
    def _compute_angle(atom1, atom2, atom3):
        """Calculate angle between three points in degrees."""
        # Define vectors between atoms
        ba = atom2 - atom1
        bc = atom3 - atom2

        # calculate dot product and angle
        angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
        angle = round(angle, 15)
        angle = np.arccos(angle)

        return 180 - np.degrees(angle)

    @staticmethod
    def _compute_diheral(atom1, atom2, atom3, atom4):
        """Calulate dihedral angle between for atoms in degrees."""
        b0 = -1.0*(atom2 - atom1)
        b1 = atom3 - atom2
        b2 = atom4 - atom3

        # normalize b1 so that it does not influence magnitude of vector
        # rejections that come next
        b1 /= np.linalg.norm(b1)

        # vector rejections
        # v = projection of b0 onto plane perpendicular to b1
        #   = b0 minus component that aligns with b1
        # w = projection of b2 onto plane perpendicular to b1
        #   = b2 minus component that aligns with b1
        v = b0 - np.dot(b0, b1)*b1
        w = b2 - np.dot(b2, b1)*b1

        # angle between v and w in a plane is the torsion angle
        x = np.dot(v, w)
        y = np.dot(np.cross(b1, v), w)
        return np.degrees(np.arctan2(y, x))

    def compute_internal_coordinates(self, connection):
        """Build z-matrix for internal coordinates based on connectivity.

        Parameters
        ----------
        connection : 2D array
            Array with integer values, each column specifying atoms for
            distance, angle and dihedral angle calculation for internal
            coordinates.

        Yields
        ------
        int_coords : 2D array
            Array with with internal coorinates for distance, angle and
            dihedral angle.
        """
        self._check_connection_matrix(connection)
        self.int_coords = np.zeros(self.connection.shape)
        for idx, con_row in enumerate(self.connection):
            self.int_coords[idx, 0] = np.linalg.norm(
                                        self.coordinates[idx, :] -
                                        self.coordinates[con_row[0]-1])
            if idx > 0:
                self.int_coords[idx, 1] = self._compute_angle(
                                        self.coordinates[idx, :],
                                        self.coordinates[con_row[0]-1],
                                        self.coordinates[con_row[1]-1])
            if idx > 1:
                self.int_coords[idx, 2] = self._compute_diheral(
                                        self.coordinates[idx, :],
                                        self.coordinates[con_row[0]-1],
                                        self.coordinates[con_row[1]-1],
                                        self.coordinates[con_row[2]-1])
        mask = self.connection == 0
        self.int_coords[mask] = 0

    def create_interal_coordinates(self, connection, outputfile=None,
                                   order=None):
        """Create an input file with coordinates in the principal axes system.

        Parameters
        ----------
        connection : 2D array
            Array with integer values, each column specifying atoms for
            distance, angle and dihedral angle calculation for internal
            coordinates. Connection assumes reordered atoms.

        output_file : str, optional
            Name of the output file. If none is given, the filename given to
            the class will be used with an .inp as a file name.

        order : lst, optional
            List of integers for the rearanging the coordinates. List the
            current atom number in the desired order. Default is None.

        Yields
        ------
        Text file which can be used as an orca input file with the molecular
        coordinates in the principal axes system.
        """
        # set the outputfile for to write the file
        if outputfile is None:
            outputfile = '{}-int-coord.int'.format(
                os.path.splitext(self.filename)[0])
        # Rearange atom order if given
        if order is not None:
            self.reorder_coordinates(order)
        # compute the internal coordinates based on the connection matrix
        self.compute_internal_coordinates(connection)
        # write the file
        with open(outputfile, 'w', encoding='utf-8') as file:
            file.write('# Interal coordinates\n')
            file.write(f'{int(self.natoms)}\n')
            for idx, name in enumerate(self.names):
                name = name.rjust(4, ' ')
                atnum = str(self.atnum[idx]).ljust(2, ' ')
                con1 = str(self.connection[idx, 0]).rjust(7, ' ')
                con2 = str(self.connection[idx, 1]).rjust(4, ' ')
                con3 = str(self.connection[idx, 2]).rjust(4, ' ')
                bon = '{:.7f}'.format(
                                self.int_coords[idx, 0]).rjust(13, ' ')
                ang = '{:.7f}'.format(
                                self.int_coords[idx, 1]).rjust(16, ' ')
                dih = '{:.7f}'.format(
                                self.int_coords[idx, 2]).rjust(15, ' ')
                mass = '{:.7f}'.format(
                                self.mass[idx]).rjust(13, ' ')
                file.write(atnum + con1 + con2 + con3 + bon + ang +
                           dih + mass + name + '\n')
        print('Input file created successfully at: ', outputfile)

    def format_output(self, outputfile=None, Bailey=False):
        """
        Write transformed version of the orca output for fitting applications.

        Parameters
        ----------
        outputfile : str, optional
            Name of the output file. If none is given, the filename given to
            the class will be used with an .inp as a file name.

        Bailey : boolean, optional
            If Bailey = True, Q_eff method from William Bailey is used,
            Check for the correct calculation method of the nucleus.
        """
        self.compute_ABC_coord()
        self.transform_chi_tensor(Bailey)
        if outputfile is None:
            outputfile = '{}_res.out'.format(
                os.path.splitext(self.filename)[0])

        with open(outputfile, 'w', encoding='utf-8') as file:
            if 'E_tot' in self.results_dict.keys():
                file.write('TOTAL SP ENERGY\n------------\n\n')
                file.write('E: {} E_h\n\n'.format(
                    self.results_dict['E_tot'][0]))
                file.write('E: {} eV\n\n'.format(
                    self.results_dict['E_tot'][0] * 27.211396641308))
                file.write('------------------------------------------' +
                           '---------\n')
                file.write('\n\n')
            if 'rot_const_MHz' in self.results_dict.keys():
                file.write('Rotational constants\n--------------------\n\n')
                file.write('A, B, C [MHz] (Orca): {}\n\n'.format(
                    self.results_dict['rot_const_MHz']))
                file.write('A, B, C [MHz] (Geometry): {}\n\n'.format(
                    self.rot_const))
                file.write('--------------------------------------------' +
                           '-------\n')
                file.write('\n\n')
            if 'mu_xyz' in self.results_dict.keys():
                file.write('Total dipole moment\n-------------------\n\n')
                file.write('\u03BC_xyz [a. u.]: {}\n\n'. format(
                    self.results_dict['mu_xyz']))
                if 'mu_abc_debye' in self.results_dict.keys():
                    file.write('\u03BC_abc [Debye]: {}\n\n'. format(
                        self.results_dict['mu_abc_debye']))
                file.write('------------------------------------------' +
                           '---------\n')
                file.write('\n\n')
            if 'quad_nuc_name' in self.results_dict.keys():
                file.write('Hyperfine structure\n-------------------\n\n')
                for i, nuc_name in enumerate(
                        self.results_dict['quad_nuc_name']):
                    file.write(f'{nuc_name}\n\n')
                    file.write('Q [barn]: {}\n'.format(
                        self.results_dict[nuc_name][0]))
                    file.write('Q [m**2]: {}\n\n'.format(
                        self.results_dict[nuc_name][1]))

                    file.write('Raw electric field gradient [a.u.]:\n')
                    file.write('{}\n\n'.format(self.results_dict[nuc_name][2]))

                    file.write('Rotated nuclear quadrupole' +
                               'coupling tensor [MHz]:\n')
                    file.write('{}\n\n'.format(self.results_dict[nuc_name][5]))

                    # Chi values for pickett
                    Chi_aa, Chi_bb_cc, Chi_bb_cc_4 = self.chi_calc(
                        self.results_dict[nuc_name][5])
                    file.write('3/2 \u03C7_aa [MHz]: {}\n'.format(Chi_aa))
                    file.write('(\u03C7_bb-\u03C7_cc)/4 [MHz]: {}\n'.format(
                        Chi_bb_cc_4))
                    file.write('\u03C7_bb-\u03C7_cc [MHz]: {}\n'.format(
                        Chi_bb_cc))
                    file.write('\n\n')

        print(f'Output has been printed to {outputfile}')


def information_to_excel(filelist, outputfile=None, Bailey=False):
    """
    Wrapper to print information from a list of orca or strfit to excel.

    Parameters
    ----------
    outputfile : str, optional
        Name of the output file. If none is given, the filename given to
        the class will be used with an .inp as a file name.

    Bailey : boolean, optional
        If Bailey = True, Q_eff method from William Bailey is used,
        Check for the correct calculation method of the nucleus.

    Returns
    -------
    df : dataframe
        Dataframe containing the information that was read form Orca output.
    """
    molecule = Geometry(filelist[0])
    if 'Orca' in molecule.file_type:
        df_s, m_list = orca_info_to_excel(filelist, Bailey)
        if len(m_list) > 0:
            df_m, s_list = m_job_orca_info_to_excel(filelist, Bailey)
            if df_s is not 'no singles':
                df = pd.concat([df_m, df_s])
            else:
                df = df_m
        else:
            df = df_s
        if outputfile is None:
            outputfile = '{}\\orca-table.xlsx'.format(
                            os.path.dirname(filelist[0]))
        df.to_excel(outputfile)
    elif 'Strfit output' in molecule.file_type:
        df = stf_info_to_excel(filelist)
        if outputfile is None:
            outputfile = '{}\\stfit-table.xlsx'.format(
                            os.path.dirname(filelist[0]))
        df.to_excel(outputfile)
    else:
        df = xyz_info_to_excel(filelist)
        if outputfile is None:
            outputfile = '{}\\xyz-table.xlsx'.format(
                            os.path.dirname(filelist[0]))
        df.to_excel(outputfile)
    return df


def chi_sorted_eigenvalues(chi, nuc):
    """
    Sort the eigenvalues of chi tensor to mapping scheme.

    Calulate the eigenvalues of chi and check the nucleus. The mapping sheme
    for the halogenes is known, otherwise it needs to be set for each new
    nucleus.

    Parameters
    ----------
    chi : (3, 3) np.array
        Contains the chi tensor in the principal axis frame.
    nuc : str
        Name of the nucleus as a string.

    Returns
    -------
    eigenv : np.arry
        array with three values containing the sorted eigenvalues of the chi
        tensor.
    """
    eigenv = np.array(np.linalg.eigh(chi)[0])
    eigenv = np.sort(eigenv)
    nuc_name = ''.join(re.split('[^a-zA-Z]*', nuc))
    if nuc_name in eigenv_map_sheme.keys():
        eigenv = sorted(eigenv, key=lambda x: abs(x))
        eigenv = [eigenv[i] for i in eigenv_map_sheme[nuc_name]]
        print('Nucleus recognized. XYZ mapping set as' +
              f'{np.array(eigenv_map_sheme[nuc_name]) + 1}')
        return eigenv
    else:
        x = input(f'Mapping for the {nuc_name} nucleus required. Please ' +
                  'enter the numbers 1, 2 and 3, where 1 corresponds to the ' +
                  'smallest eigenvalue.\n X:')
        y = input('Y:')
        z = input('Z:')
        try:
            x = int(x)
            y = int(y)
            z = int(z)
        except ValueError:
            pass
        if x != 1:
            if x != 2:
                if x != 3:
                    print(f'x = {x} value invalid')
                    eigenv = [eigenv[i] for i in eigenv_map_sheme['default']]
                    eigenv_map_sheme[nuc_name] = eigenv_map_sheme['default']
                    print('Invalid mapping. XYZ mapping set to default as ' +
                          f'{np.array(eigenv_map_sheme["default"]) + 1}')
                    return eigenv
        if y != 1:
            if y != 2:
                if y != 3:
                    print(f'y = {y} value invalid')
                    eigenv = [eigenv[i] for i in eigenv_map_sheme['default']]
                    eigenv_map_sheme[nuc_name] = eigenv_map_sheme['default']
                    print('Invalid mapping. XYZ mapping set to default as ' +
                          f'{np.array(eigenv_map_sheme["default"]) + 1}')
                    return eigenv
        if z != 1:
            if z != 2:
                if z != 3:
                    print(f'z = {z} value invalid')
                    eigenv = [eigenv[i] for i in eigenv_map_sheme['default']]
                    eigenv_map_sheme[nuc_name] = eigenv_map_sheme['default']
                    print('Invalid mapping. XYZ mapping set to default as ' +
                          f'{np.array(eigenv_map_sheme["default"]) + 1}')
                    return eigenv
        if x + y + z != 6:
            print('x, y, z value invalid')
            eigenv = [eigenv[i] for i in eigenv_map_sheme['default']]
            eigenv_map_sheme[nuc_name] = eigenv_map_sheme['default']
            print('Invalid mapping. XYZ mapping set to default as ' +
                  f'{np.array(eigenv_map_sheme["default"]) + 1}')
            return eigenv
        order = np.array([x, y, z]) - 1
        eigenv = [eigenv[i] for i in order]
        eigenv_map_sheme[nuc_name] = order
        print(f'XYZ mapping set for {nuc_name} as ' +
              f'{np.array(eigenv_map_sheme[nuc_name]) + 1}')
        return eigenv


def orca_info_to_excel(filelist, Bailey=False):
    """
    Bundle information of orca outputfiles in excel table.

    Parameters
    ----------
    filelist : lst
        List of complete paths to orca outputfiles which are to be bundled in
        an excel data table.

    Bailey : boolean, optional
            If Bailey = True, Q_eff method from William Bailey is used,
            Check for the correct calculation method of the nucleus.

    Yields
    ------
    df : dataframe
        Dataframe printed into an excel table with the results of the orca
        calculations. It contains:
        Filename, rotational constants in MHz, SCF energy in Eh, transition
        dipole moment in Debye, name of each nucleus, chi tensor in MHz,
        conversions of chi tensor for Pickett Hamiltonian.

    mJobs_filelist : lst
        List of files containing multiple jobs for usage in wrapper function.
    """

    data = []

    reg_column_names = ['filename', 'directory', 'A / MHz', 'B / MHz',
                        'C / MHz', 'E / Eh', 'mu_a / D',
                        'mu_b / D', 'mu_c / D', 'opt_convergence']

    nuc_col = ['nuc_name', 'chi_aa', 'chi_bb', 'chi_cc', 'chi_ab', 'chi_ac',
               'chi_bc', '3/2 chi_aa', '(chi_bb - chi_cc) / 4 ',
               'chi_bb - chi_cc', 'chi_xx', 'chi_yy', 'chi_zz']

    mJobs_filelist = []
    for file in filelist:
        data_mol = []
        print(file)
        file = Path(file)
        try:
            molecule = Geometry(file)
        except AttributeError:
            warn(f'File {file} not read!')
            continue
        try:
            getattr(molecule, 'n_jobs')
        except AttributeError:
            pass
        else:
            warn(f'File {file} has multiple jobs and was skipped!')
            mJobs_filelist.append(file)
            continue

        data_mol.append(file.stem)
        data_mol.append(
            os.path.split(file)[0])
        molecule.compute_ABC_coord()
        molecule.transform_chi_tensor(Bailey)
        for i in np.arange(3):
            data_mol.append(molecule.rot_const[i])
        data_mol.append(molecule.results_dict['E_tot'][0])
        for i in np.arange(3):
            data_mol.append(molecule.results_dict['mu_abc_debye'][i])
        if 'opt_convergence' in molecule.results_dict:
            data_mol.append(molecule.results_dict['opt_convergence'])
        else:
            data_mol.append('SP calc')
        if 'quad_nuc_name' not in molecule.results_dict:
            data.append(data_mol)
            continue
        for idx, nuc in enumerate(molecule.results_dict['quad_nuc_name']):
            data_mol.append(nuc)
            data_mol.append(molecule.results_dict[nuc][5][0, 0])
            data_mol.append(molecule.results_dict[nuc][5][1, 1])
            data_mol.append(molecule.results_dict[nuc][5][2, 2])
            data_mol.append(molecule.results_dict[nuc][5][0, 1])
            data_mol.append(molecule.results_dict[nuc][5][0, 2])
            data_mol.append(molecule.results_dict[nuc][5][1, 2])
            Chi_aa, Chi_bb_cc, Chi_bb_cc_4 = Geometry.chi_calc(
                                             molecule.results_dict[nuc][5])
            data_mol.append(Chi_aa)
            data_mol.append(Chi_bb_cc_4)
            data_mol.append(Chi_bb_cc)
            Chi_xx, Chi_yy, Chi_zz = chi_sorted_eigenvalues(
                                        molecule.results_dict[nuc][5], nuc)
            data_mol.append(Chi_xx)
            data_mol.append(Chi_yy)
            data_mol.append(Chi_zz)
        data.append(data_mol)

    pad_token = np.nan
    padded_data = np.array(
                list(zip(*itertools.zip_longest(*data, fillvalue=pad_token))))
    try:
        max_nuc_num = (padded_data.shape[1] - len(reg_column_names)) / len(
            nuc_col)
    except IndexError:
        return 'no singles', mJobs_filelist
    nuc_names = []
    for idx in np.arange(1, max_nuc_num + 1):
        for label in nuc_col:
            nuc_names.append(label + '_{}'.format(int(idx)))
    df = pd.DataFrame(padded_data, columns=reg_column_names + nuc_names)
    return df, mJobs_filelist


def m_job_orca_info_to_excel(filelist, Bailey=False):
    """
    Bundle information of orca outputfiles with multiple jobs in excel table.

    Parameters
    ----------
    filelist : lst
        List of complete paths to orca outputfiles which are to be bundled in
        an excel data table.

    Bailey : boolean, optional
            If Bailey = True, Q_eff method from William Bailey is used,
            Check for the correct calculation method of the nucleus.

    Yields
    ------
    df : dataframe
        Dataframe printed into an excel table with the results of the orca
        calculations. It contains:
        Filename, rotational constants in MHz, SCF energy in Eh, transition
        dipole moment in Debye, name of each nucleus, chi tensor in MHz,
        conversions of chi tensor for Pickett Hamiltonian.
    sJobs_filelist : lst
        List of files containing a single job for usage in wrapper function.
    """
    data = []

    reg_column_names = ['filename', 'directory', 'n_job', 'job_name',
                        'A / MHz', 'B / MHz', 'C / MHz', 'E / Eh', 'mu_a / D',
                        'mu_b / D', 'mu_c / D', 'opt_convergence']

    nuc_col = ['nuc_name', 'chi_aa', 'chi_bb', 'chi_cc', 'chi_ab', 'chi_ac',
               'chi_bc', '3/2 chi_aa', '(chi_bb - chi_cc) / 4 ',
               'chi_bb - chi_cc', 'chi_xx', 'chi_yy', 'chi_zz']

    sJobs_filelist = []
    for file in filelist:
        print(file)
        file = Path(file)
        try:
            molecule = Geometry(file)
        except AttributeError:
            warn(f'File {file} not read!')
            continue
        try:
            molecule.n_jobs
        except AttributeError:
            warn(f'File {file} does not contain multiple jobs, hence excluded')
            sJobs_filelist.append(file)
            continue
        for job in np.arange(1, molecule.n_jobs + 1):
            data_mol = []
            # filename
            data_mol.append(file.stem)
            # directory
            data_mol.append(
                os.path.splitext(file)[0])
            # job number
            data_mol.append(job)
            # job name
            data_mol.append(molecule.results_dict[job]['job_name'])
            # read coordinates for job
            molecule = Geometry(file, job_num=job)
            molecule.compute_ABC_coord()
            molecule._m_transform_chi_tensor(job, Bailey)
            # rotational constants
            for i in np.arange(3):
                data_mol.append(molecule.rot_const[i])
            data_mol.append(molecule.results_dict[job]['E_tot'][0])
            # Dipole Moment
            for i in np.arange(3):
                data_mol.append(molecule.results_dict[job]['mu_abc_debye'][i])
            # convergence criterion
            if 'opt_convergence' in molecule.results_dict[job]:
                data_mol.append(molecule.results_dict[job]['opt_convergence'])
            else:
                data_mol.append('SP calc')
            # EFG for nuclei
            if 'quad_nuc_name' not in molecule.results_dict[job]:
                data.append(data_mol)
                continue
            for idx, nuc in enumerate(molecule.results_dict[job][
                                                            'quad_nuc_name']):
                data_mol.append(nuc)
                data_mol.append(molecule.results_dict[job][nuc][5][0, 0])
                data_mol.append(molecule.results_dict[job][nuc][5][1, 1])
                data_mol.append(molecule.results_dict[job][nuc][5][2, 2])
                data_mol.append(molecule.results_dict[job][nuc][5][0, 1])
                data_mol.append(molecule.results_dict[job][nuc][5][0, 2])
                data_mol.append(molecule.results_dict[job][nuc][5][1, 2])
                Chi_aa, Chi_bb_cc, Chi_bb_cc_4 = Geometry.chi_calc(
                                            molecule.results_dict[job][nuc][5])
                data_mol.append(Chi_aa)
                data_mol.append(Chi_bb_cc_4)
                data_mol.append(Chi_bb_cc)
                Chi_xx, Chi_yy, Chi_zz = chi_sorted_eigenvalues(
                                    molecule.results_dict[job][nuc][5], nuc)
                data_mol.append(Chi_xx)
                data_mol.append(Chi_yy)
                data_mol.append(Chi_zz)
            data.append(data_mol)
    pad_token = np.nan
    padded_data = np.array(
                list(zip(*itertools.zip_longest(*data,
                                                fillvalue=pad_token))))
    max_nuc_num = (padded_data.shape[1] - len(reg_column_names)) / len(nuc_col)
    nuc_names = []
    for idx in np.arange(1, max_nuc_num + 1):
        for label in nuc_col:
            nuc_names.append(label + '_{}'.format(int(idx)))
    df = pd.DataFrame(padded_data, columns=reg_column_names + nuc_names)
    return df, sJobs_filelist


def stf_info_to_excel(filelist):
    """
    Bundle information of str outputfiles in excel table.

    Parameters
    ----------
    filelist : lst
        List of complete paths to strfit outputfiles which are to be bundled in
        an excel data table.

    Yields
    df : dataframe
        Dataframe printed into an excel table with the results of the strfit
        calculation.
    """
    data_dict = {'convergence': [], 'filename': [], 'directory': []}
    for file in filelist:
        print(file)
        file = Path(file)
        try:
            molecule = Geometry(file)
        except AttributeError:
            warn(f'File {file} not read!')
            continue
        data_dict['filename'].append(file.stem)
        data_dict['directory'].append(
             os.path.split(os.path.split(os.path.splitext(file)[0])[0])[1])
        for key in molecule.results_dict.keys():
            # check if list already exists
            if key not in data_dict:
                data_dict[key] = []
                # check if value has uncertainty
                if np.array(molecule.results_dict[key]).size == 2:
                    data_dict['Delta ' + key] = []
            # check if there is a length difference between the convergence
            # criterion, this prevents from files which didn't converge to mess
            # up the order, by padding it with nan values
            len_diff = len(data_dict['filename']) - 1 - len(data_dict[key])
            if len_diff > 0:
                pad_list = [np.nan for i in np.arange(len_diff)]
                data_dict[key].append(*pad_list)
                if np.array(molecule.results_dict[key]).size == 2:
                    data_dict['Delta ' + key].append(*pad_list)
            if np.array(molecule.results_dict[key]).size == 2:
                data_dict[key].append(molecule.results_dict[key][0])
                data_dict['Delta ' + key].append(molecule.results_dict[key][1])
            else:
                data_dict[key].append(molecule.results_dict[key])
    df = pd.DataFrame(data_dict)
    return df


def xyz_info_to_excel(filelist):
    """
    Bundle information of xyz in excel table.

    Parameters
    ----------
    filelist : lst
        List of complete paths to strfit outputfiles which are to be bundled in
        an excel data table.

    Yields
    df : dataframe
        Dataframe printed into an excel table with the results of the strfit
        calculation.
    """
    data = []

    column_names = ['filename', 'directory', 'A / MHz', 'B / MHz', 'C / MHz']
    for file in filelist:
        data_mol = []
        print(file)
        file = Path(file)
        try:
            molecule = Geometry(file)
        except AttributeError:
            warn(f'File {file} not read!')
            continue

        data_mol.append(file.stem)
        data_mol.append(
            os.path.split(file)[0])
        molecule.compute_ABC_coord()
        for i in np.arange(3):
            data_mol.append(molecule.rot_const[i])
        data.append(data_mol)
    df = pd.DataFrame(data, columns=column_names)
    return df