# -*- coding: utf-8 -*-
"""
Created on Mon Sep  4 12:55:30 2023

@author: kempken1
"""
import numpy as np
import sys
sys.path.insert(0, 'D:\\orca-output')
from GeometryClass import Geometry
from Internal_H2_rotation import (get_idx_of_X2, fit_plane_to_molecule,
                                  rotate_H2_parallel_to_plane,
                                  projection_to_plane, calculate_rot_barrier,
                                  calc_int_angular_momentum,
                                  calc_delta_and_epsilon,
                                  rot_constants_XIAM_pickett)

mol = Geometry('D:\\ownCloud\\Obenchain\\Research\\fluorobenzaldehydes\\o-fluorobenzaldehyde\\calculations\\2-t-FBz\\2-t-FBz-H2\\orca 5.0.4\\B2PLYP-D3-def2-TZVPP\\B2PLYP-D3-def2-TZVPP.xyz')


###############################################################################
###############################################################################

"Extract the indices of two connected atoms in a molecule from an .xyz file"

" -> get_idx_of_X2(Geometry, element1, element2, atom1_idx = None) "

###############################################################################

'''
The function is used on a set of atomic coordiantes and an accompanying set
of the respective elements as they can be extracted from an xyz file. It
allows to extract the indices of two atoms in the coordinates set if they
are bound to each other. The extraction is based on finding the closest
neighbour of each atom combined with the criterion that the atom
corresponds to the element of interest. The function can be used in two
modes:

    1. Both indices are unknown:
        In this case, the extraction is only possible if the two atoms are
        directly connected to each other and the element pair is unique
        (e.g. if we look for the indices of a C atom and an H atom, forming
        a C-H bond, this requires that there is no other C-H bond in the
        molecule.)

    2. The index of one of the atoms (atom1, element1) is known:
        In this case, the extraction is possible if the known atom is bound
        to only one atom of element2.

If the two atoms of interest are not directly connected to each other in
the molecule, there might be the possiblility to extract the indices step-
wise by calling the function in a series in the second mode (if e.g. the
indices of the halogen-bound C atoms in 1-Chloro-3-fluorobenzene shall be
extracted, one can run get_idx_of_X2 with the [Cl, C] combination and with
the [F, C] combination).

'''

# Example use of the function:

atom1_idx, atom2_idx = get_idx_of_X2(mol, 'H', 'H')

###############################################################################
###############################################################################

" Fit a plane to a molecule"

" -> fit_plane_to_molecule(coords) "

###############################################################################

'''
The function fits a plane to the molecule in a least-squares fit. The fit
parameters are returned as well as the orthonormalized basis vectors of the
plane.
'''

# Example use of the function:

coords = np.array([[-0.51221, 0.120799, -0.04660],
                  [-0.03555, 1.43629, -0.07255],
                  [1.32398, 1.69719, -0.09452],
                  [2.23305, 0.63836, -0.09110],
                  [1.78667, -0.67721, -0.06590],
                  [0.42290, -0.91097, -0.04471],
                  [-1.96710, -0.14606, -0.01847],
                  [-0.76408, 2.23400, -0.07241],
                  [1.68053, 2.71611, -0.11369],
                  [3.29485, 0.83693, -0.10702],
                  [2.47007, -1.51251, -0.06207],
                  [-2.80409, 0.73216, -0.02011],
                  [-2.25992, -1.20570, 0.00504],
                  [-0.00619, -2.18771, -0.01976]])

# params, e1, e2 = fit_plane_to_molecule(coords)
# print(params, e1, e2)


###############################################################################
###############################################################################

" Create multiple modifications of the structure of an H2-Fluorobenzaldehyde  \
complex, stored as .xyz files -> create structures with the H2 parallel to    \
the aromatic plane and at different torsional angles with an anchor vector"

" -> rotate_H2_parallel_to_plane(directory, CoM, anchor_vector, side_vector, n, idx_H1, idx_H2, all_coords, all_elements, subfolder) "

###############################################################################

'''
The function generates a set of .xyz files of an X2-aromate complex. The
different structures are derived from a template structure from which the
X2 unit is extracted. The X2 unit is rotated to an alignment parallel to
the aromatic plane and parallel to an anchor vector in the plane (e.g. a
specific bond in the aromatic system). The X2's center of mass coordinates
are maintained. From this alignment, the torsional angle between the X-X
bond vector and the anchor vector is varied successively up to an angle of
180°. In the course of this, the parallel alignment of the X2 to the plane
and the X2's center of mass are maintained. To meet these conditions, two
rotation directions are possible. The one direction is chosen which leads
to an approximation of the alignment of the X-X bond
vector to the alignment of a side_vector (e.g. another bond in the aromatic
plane) at the beginning of the torsional angle variation. The resulting
structures are stored as .xyz files.
'''

# Example use of the function:

directory = 'D:\\ownCloud\\Obenchain\\Research\\fluorobenzaldehydes\\o-fluorobenzaldehyde\\calculations\\2-t-FBz\\2-t-FBz-H2\\orca 5.0.4\\B2PLYP-D3-def2-TZVPP'
anchor_vector = mol.coordinates[6]-mol.coordinates[0]
side_vector = mol.coordinates[13]-mol.coordinates[5]
n = np.linspace(144-5, 144+5, 11)
idx_H1 = atom1_idx
idx_H2 = atom2_idx
subfolder = 'H2-rotation-scan'

rotate_H2_parallel_to_plane(directory, mol, anchor_vector, side_vector, n,
                            idx_H1, idx_H2, subfolder)


###############################################################################
###############################################################################

" Project point to plane "
" -> projection_to_plane(e1, e2, z_offset, all_coords, point) "

###############################################################################

'''
The function projects a point outside a plane in a cartesian coordinate
system to the plane. It returns the coordinates of the projected point and
its distance to the input point.
'''

# Example use of the function:

e1 = np.array([ 0.99989632,  0., -0.01439975])
e2 = np.array([-2.13283606e-04,  9.99890302e-01, -1.48100857e-02])
z_offset = -0.04985439
coords = np.array([[-0.51221, 0.120799, -0.04660], 
                  [-0.03555, 1.43629, -0.07255],
                  [1.32398, 1.69719,-0.09452],
                  [2.23305, 0.63836, -0.09110],
                  [1.78667, -0.67721, -0.06590],
                  [0.42290, -0.91097, -0.04471],
                  [-1.96710, -0.14606, -0.01847],
                  [-0.76408, 2.23400, -0.07241],
                  [1.68053, 2.71611, -0.11369],
                  [3.29485, 0.83693, -0.10702],
                  [2.47007, -1.51251, -0.06207],
                  [-2.80409, 0.73216, -0.02011],
                  [-2.25992, -1.20570, 0.00504],
                  [-0.00619, -2.18771, -0.01976],
                  [0.74552, 0.36887, 3.44339],
                  [0.77784, 0.52861, 2.72105]])
point = np.array([0.76168, 0.44874, 3.08222])

# projected_point_coords, d_point_to_proj_point = projection_to_plane(e1, e2, z_offset, coords, point)
# print(projected_point_coords, d_point_to_proj_point)

###############################################################################
###############################################################################

" Calculate H2 rotational barrier in complex "
" -> calculate_rot_barrier(cwd, opt_output_file) "

###############################################################################

'''
The function returns the rotational barrier in different units. For each 
structure (optimized and single point) the energy of the system is extracted
from the respective orca output file. The energy of the optimized structure
is the smallest by definition. The single point structure with the smallest
energy is considered as the structure of the transition state of the 
rotation. Its energy difference to the optimized structure energy is 
interpreted as rotational barrier.
'''

# Example use of the function:

# opt_out_file = 'B2PLYP-D3-def2-TZVPP-OPT.out'
# path = 'D:\\OwnCloud\\Research\\fluorobenzaldehydes\\o-fluorobenzaldehyde\\calculations\\2-t-FBz\\2-t-FBz-H2\\orca 5.0.4\\' + opt_out_file[:-8] + '\\single point calcs'

# barrier_cm, barrier_GHz = calculate_rot_barrier(path, opt_out_file)[:2]
# barrier_Hartree, min_SP = calculate_rot_barrier(SP_path, opt_path, opt_out_file)[2:4]
# max_SP, barrier_max_cm = calculate_rot_barrier(SP_path, opt_path, opt_out_file)[4:6]
# SP_energy, opt_energy, output_files = calculate_rot_barrier(SP_path, opt_path, opt_out_file)[6:9]


###############################################################################
###############################################################################

" Calculate H2 internal angular momentum in a complex "
" -> calc_int_angular_momentum(H2_bond, arom_plane_coeffs) "

###############################################################################

'''
The function calculates a vector parallel to the angular momentum P for H2 
internal rotation in a complex.
'''
H2_bond = np.array([-0.03232, -0.15974,  0.72234])
coeff = np.array([-0.01440124, -0.01481478, -0.04985439])

# Example use of the function:
# n_plane, P = calc_int_angular_momentum(H2_bond, coeff)


###############################################################################
###############################################################################

" Convert pickett rotational constants to XIAM rotational constants or vice   \
  versa "
" -> rot_constants_XIAM_pickett(rot_constants) "

###############################################################################

'''
Depending on the input (0 or 1), the function converts pickett A, B, C 
rotational constants to XIAM rotational constants or vice versa and prints
the result.
'''

# Example use of the function:
# rot_constants_XIAM_pickett(np.array([1259.93, 922.01, 759.94]))
# input: '0'
    

###############################################################################
###############################################################################


" calculate delta and epsilon for XIAM fit "
" -> calc_delta_and_epsilon(Rot_matrix, n_plane, P) "

###############################################################################

'''
The function derives the delta and epsilon value from the predicted struc-
ture. Delta and epsilon are parameters in the Coriolis coupling terms of
the rotational Hamiltonian used by XIAM. Delta represents the angle between
the H2 rotational angular momentum and the a-axis. Epsilon equals the angle
between H2 angular momentum projection to the bc plane and the b-axis.
'''

# Example use of the function

# Rotation_matrix = np.array([0.2340, 0.3245, 1.3223], 
#                            [0.4624, 0.7654, 0.1094],
#                            [0.4246, 0.2222,-1.4123])

# P = np.array([1.0, 0, 1.0])
# n_plane = np.array([0, 1.0, 0])

# delta, epsilon = calc_delta_and_epsilon(Rotation_matrix, n_plane, P)


