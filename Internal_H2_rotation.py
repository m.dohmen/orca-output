# -*- coding: utf-8 -*-
"""
Created on Wed May  3 09:57:00 2023

@author: kempken1
"""

# from GeometryClass import Geometry  # , compute_COM
import scipy.linalg
import numpy.linalg
from pathlib import Path
import matplotlib.pyplot as plt
import os
import scipy.constants as sc
import numpy as np
import math
from masses import masses


def get_idx_of_X2(Geometry, element1, element2, atom1_idx=None):
    '''
    The function is used on a set of atomic coordiantes and an accompanying set
    of the respective elements as they can be extracted from an xyz file. It
    allows to extract the indices of two atoms in the coordinates set if they
    are bound to each other. The extraction is based on finding the closest
    neighbour of each atom combined with the criterion that the atom
    corresponds to the element of interest. The function can be used in two
    modes:

        1. Both indices are unknown:
            In this case, the extraction is only possible if the two atoms are
            directly connected to each other and the element pair is unique
            (e.g. if we look for the indices of a C atom and an H atom, forming
            a C-H bond, this requires that there is no other C-H bond in the
            molecule.)

        2. The index of one of the atoms (atom1, element1) is known:
            In this case, the extraction is possible if the known atom is bound
            to only one atom of element2.

    If the two atoms of interest are not directly connected to each other in
    the molecule, there might be the possiblility to extract the indices step-
    wise by calling the function in a series in the second mode (if e.g. the
    indices of the halogen-bound C atoms in 1-Chloro-3-fluorobenzene shall be
    extracted, one can run get_idx_of_X2 with the [Cl, C] combination and with
    the [F, C] combination).


    Parameters
    ----------
    Geometry : GeometryClass object
        Contains molecular geometry from xyz or outputfile.
    element1 : String
        Element label of the first X atom.
    element2 : String
        Element label of the second X atom.

    Returns
    -------
    atom_idx1 : Integer
        Index of the sublist in all_coords which represents the coordinates of
        atom1.
    atom_idx2 : Integer
       Index of the sublist in all_coords which represents the coordinates of
       atom2.

    '''

    # Find the index of atom1 and atom2
    if atom1_idx is None:

        # For each atom, the atom with the smallest distance to it will be
        # determined. The indices of these closest neighbours in the xyz file
        # will be stored in the following array.
        idx_closest_neighbour = np.zeros(len(Geometry.coordinates))

        # Identification of the closest neighbours and their indices
        for i in range(Geometry.natoms):
            idx_neighbour = 0
            d_neighbour = np.nan
            for j in range(Geometry.natoms):
                if i == j:
                    continue
                d = np.linalg.norm(Geometry.coordinates[i] -
                                   Geometry.coordinates[j])
                if not d > d_neighbour:
                    idx_neighbour = j
                    d_neighbour = d

            idx_closest_neighbour[i] = idx_neighbour

        # define indicator variables which indicate if the seach was successful
        # or not.
        atom1_idx = -1
        atom2_idx = -1

        for i in range(Geometry.natoms):
            if Geometry.names[i] == element1:
                if Geometry.names[int(idx_closest_neighbour[i])] == element2:
                    atom1_idx = i
                    atom2_idx = int(idx_closest_neighbour[i])
        if atom1_idx == -1:
            print('Idx atom 1 not found. :(')
        if atom2_idx == -1:
            print('Idx atom 2 not found. :(')
        if element1 != element2:
            if Geometry.names[atom1_idx] != element1:
                saveIDX = atom1_idx
                atom1_idx = atom2_idx
                atom2_idx = saveIDX

    else:
        # define indicator variable which indicate if the seach was successful
        # or not.
        atom2_idx = -1

        d_neighbour = np.nan

        # Find the index of atom2. The index of atom1 is known.
        for i in range(Geometry.natoms):
            if i == atom1_idx:
                continue

            # Identification of the index of the atom of element2 which is
            # closest to atom1:
            if Geometry.names[i] == element2:
                d = np.linalg.norm(Geometry.coordinates[i] -
                                   Geometry.coordinates[atom1_idx])
                if not d > d_neighbour:
                    atom2_idx = i
                    d_neighbour = d

        if atom2_idx == -1:
            print('Idx atom 2 not found. :(')
    if element1 != element2:
        idx = np.array([atom1_idx, atom2_idx])
        if Geometry.names[atom1_idx] != element1:
            idx = np.array([atom2_idx, atom1_idx])
    else:
        idx = np.array([atom1_idx, atom2_idx])
    return idx[0], idx[1]


def fit_plane_to_molecule(coords, plot=False):
    '''
    The function fits a plane to the molecule in a least-squares fit. The fit
    parameters are returned as well as the orthonormalized basis vectors of the
    plane.

    Parameters
    ----------
    coords : list of list of floats
        A sublist contains three floats representing the cartesian coordinates
        of an atom in the system.

    Returns
    -------
    coeff : list of floats
        Floats represent the resulting values of the fit parameters. The plane
        is a function of two dimensions.
        coeff[0] represents the derivative of the function with respect to the
        first dimension.

        coeff[1] represents the derivative of the function with respect to the
        second dimension.

        coeff[2] represents the offset of the function in the third dimension.
    e1 : list of floats
        First normalized direction vector of the fitted plane in a cartesian
        coordinate system.
    e2 : list of floats
        Second normalized direction vector of the fitted plane in a cartesian
        coordinate system. e2 is perpendicular to e1.
    '''

    # define an initial plane parallel to the xy-plane at z = 1
    plane_raw = np.c_[coords.T[0], coords.T[1], np.ones(coords.T.shape[1])]

    # fit the plane to the atomic coordinates. The fit provides the plane
    # gradient in x and in y direction. The last coordinate represents the
    # plane's offset in z.
    coeff, _, _, _ = scipy.linalg.lstsq(plane_raw, coords.T[2])
    # coeff,_,_,_ = scipy.linalg.lstsq(plane_raw, coords[2])

    # create direction vectors of plane
    dx = np.array([1, 0, coeff[0]])
    dy = np.array([0, 1, coeff[1]])

    # define z-offset vector for fitted plane
    s = np.array([0, 0, coeff[2]])

    ###########################################################################
    ###############   dx and dy are not orthogonal to each other   ############
    ###############            let's change this now               ############
    ###########################################################################

    # normalize dx
    e1 = dx / np.linalg.norm(dx)

    # define vector perp to e1
    v2 = dy - (np.dot(dx, dy)/np.dot(dx, dx) * dx)

    # normalize v2
    e2 = v2 / np.linalg.norm(v2)

    if plot is True:
        # Define x and y array for plane plotting
        X = np.outer(np.linspace(min(coords.T[0])-0.2,
                                 max(coords.T[0])+0.2, 10), np.ones(10))
        Y = np.outer(np.linspace(min(coords.T[1])-0.2,
                                 max(coords.T[1])+0.2, 10), np.ones(10)).T

        # Calculate the z-coordinate for each (x,y) point in the fitted plane.
        Z = coeff[0]*X + coeff[1]*Y + coeff[2]

        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.scatter(coords.T[0], coords.T[1], coords.T[2])
        ax.plot_surface(X, Y, Z, alpha=0.3, color='r')
        # ax.scatter(0.54977887, 0.0014559,  0.04671567, color = 'r')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')

    return coeff, e1, e2


def calc_CoM(coords, names):
    '''
    The function uses the atomic coordinates and masses to calculate the center
    of mass of the system.

    Parameters
    ----------
    coords : list of lists of floats / 2D array of floats
        A sublist contains three floats representing the cartesian coordinates
        of an atom in the system.
    masses : list of floats
        Floats represent the mass of each atom in u.

    Returns
    -------
    CoM : list of floats
        Cartesian coordinates of the molecule's center of mass.

    '''
    CoM = np.zeros(3)
    mass = np.array([masses[key] for key in names])
    # print('masses_reduced:', masses_reduced)

    CoM[0] = sum(np.transpose(coords)[0] * mass) / sum(mass)
    CoM[1] = sum(np.transpose(coords)[1] * mass) / sum(mass)
    CoM[2] = sum(np.transpose(coords)[2] * mass) / sum(mass)

    return CoM


def projection_to_plane(e1, e2, z_offset, all_coords, point):
    '''
    The function projects a point outside a plane in a cartesian coordinate
    system to the plane. It returns the coordinates of the projected point and
    its distance to the input point.


    Parameters
    ----------
    e1 : list / 1D array of floats
        First normalized direction vector of the fitted plane in a cartesian
        coordinate system. Obtained from fit_plane_to_molecule.
    e2 : list / 1D array of floats
        Second normalized direction vector of the fitted plane in a cartesian
        coordinate system. Obtained from fit_plane_to_molecule.
    z_offset : float
        3rd dimension offset of the plane. Obtained from fit_plane_to_molecule.
    all_coords : list of lists of floats / 2D array of floats
        A sublist contains three floats representing the cartesian coordinates
        of an atom in the system.
    point : list / 1D array of floats
        3 floats which represent the three cartesian coordinates of a point
        outside the plane which shall be projected to the plane.

    Returns
    -------
    proj_point : list / 1D array of floats
        Floats represent cartesian coordinates of the projected point in the
        plane.
    d : float
        Distance between point and proj_point.

    '''

    # define normal vector of the plane
    n = np.cross(e1, e2)

    # Stützvektor of the plane:
    s = np.array([0, 0, z_offset])

    # define prefactor for n to change its absolute value to the CoM-plane
    # distance:
    # (plane: [x-s]*n = 0
    # straight line: x = a*n + COM
    # -> (a * n + COM)*n = s*n
    a = (-np.dot(n, point) + np.dot(n, s)) / np.dot(n, n)

    # define coordinates of the projected point
    proj_point = a * n + point
    d = np.sqrt((a*n[0])**2 + (a*n[1])**2 + (a*n[2])**2)

    return proj_point, d


def rotate_H2_parallel_to_plane(directory, Geometry, anchor_vector,
                                side_vector, angles_deg,
                                idx_H1, idx_H2, subfolder, species=''):
    '''
    The function generates a set of .xyz files of an X2-aromate complex. The
    different structures are derived from a template structure from which the
    X2 unit is extracted. The X2 unit is rotated to an alignment parallel to
    the aromatic plane and parallel to an anchor vector in the plane (e.g. a
    specific bond in the aromatic system). The X2's center of mass coordinates
    are maintained. From this alignment, the torsional angle between the X-X
    bond vector and the anchor vector is varied successively up to an angle of
    180°. In the course of this, the parallel alignment of the X2 to the plane
    and the X2's center of mass are maintained. To meet these conditions, two
    rotation directions are possible. The one direction is chosen which leads
    to an approximation of the alignment of the X-X bond
    vector to the alignment of a side_vector (e.g. another bond in the aromatic
    plane) at the beginning of the torsional angle variation. The resulting
    structures are stored as .xyz files.

    Parameters
    ----------
    directory : string
        The full path to the folder in which the calculation output files are
        stored.
    Geometry : GeometryClass object
        Contains molecular geometry from xyz or outputfile.
    anchor_vector : array of floats
        Floats represent the cartesian components of a vector which defines
        the torsional angle = 0° structure. Usually bond vector in host
        molecule.
    side_vector : array of floats
        Floats represent the cartesian components of a vector which determines
        the direction of torsional X2 rotation: The rotation proceeds in the
        direction of side_vector. Usually a bond vector in the host molecule.
    angles_deg : array of floats, integer
        Number of X2 alignemnts into which the 180° torsional angle range is
        subdivided.
    idx_H1 : integer
        Index of the sublist in the all_coords array which represents the
        cartesian coordinates of X atom 1.
    idx_H2 : integer
        Index of the sublist in the all_coords array which represents the
        cartesian coordinates of X atom 2.
    subfolder : string
        Name of the folder to which the .xyz files shall be saved.
    species : str
        aditional name in front of file, eg. species. Default is ''.
    Yields
    -------
    xyz_file

    '''
    if subfolder not in os.listdir(directory):
        os.mkdir(directory + '\\' + subfolder)
    directory_new = directory + '\\' + subfolder
    CoM = calc_CoM(Geometry.coordinates[idx_H1:idx_H2 + 1],
                   Geometry.names[idx_H1:idx_H2 + 1])

    anchor_vector = anchor_vector / np.linalg.norm(anchor_vector)
    side_vector = side_vector / np.linalg.norm(side_vector)

    method = Geometry.filename.parts[-2]

    H1 = Geometry.coordinates[idx_H1]

    # Distace of H_1 (and H_2) to the H2's center of mass:
    d_H1_CoM = np.linalg.norm(H1-CoM)

    # check if list of angles in degree is given or number of integers between
    # 0 and 180 degree
    if isinstance(angles_deg, int):
        n = angles_deg
        angles = np.linspace(0, 180, angles_deg + 1) / (360) * 2*math.pi
        # But we don't want 0° AND 180°, so we neglect the latter.
        # Therefore, angles[:-1]
        angles = angles[:-1]
    else:
        angles_deg = np.array([float(i) for i in angles_deg])
        angles = angles_deg / (360) * 2*math.pi
        n = angles.size
    # prepare arrays which will be filled with the new coordinates of the
    # rotated H_1 and H_2:
    H1_new = np.zeros((len(angles), 3))
    H2_new = np.zeros((len(angles), 3))

    # scan through different angles. But we don't want 0° AND 180°, so we
    # neglect the latter. Therefore, angles[:-1].
    for a, angle in enumerate(angles):

        # define the direction of the new CoM -> H_1 vector
        v_H1 = np.cos(angle)*anchor_vector + np.sin(angle)*side_vector

        # scale it back to the correct bond length and add the offset vector
        # (Stützvektor)
        H1_new[a] = CoM + (v_H1 / np.linalg.norm(v_H1) * d_H1_CoM)
        H2_new[a] = CoM - (v_H1 / np.linalg.norm(v_H1) * d_H1_CoM)

    # calculate angles in degrees
    angles_deg = angles/(math.pi * 2) * 360
    coords_new = Geometry.coordinates

    # integrate new H_1 and H_2 coordinates to the .xyz file for each angle:
    for i in range(n):
        for m in range(len(coords_new)):
            if m == idx_H1:
                coords_new[m] = H1_new[i]
            if m == idx_H2:
                coords_new[m] = H2_new[i]
        to_save = np.c_[Geometry.names, np.transpose(coords_new)[0],
                        np.transpose(coords_new)[1],
                        np.transpose(coords_new)[2]]

        decimal_places = '{:.0f}'.format((round(angles_deg[i], 5) -
                                          int(round(angles_deg[i], 5))) *
                                         10**2)
        np.savetxt(directory_new + '\\'  + method + species + '_' +
                   str(int(round(angles_deg[i], 5))) + '_' + decimal_places +
                   '.xyz', to_save, fmt='%s', delimiter='   ',
                   header=str(int(len(np.transpose(coords_new)[0]))) + '\n' +
                   'Coordinates for orca single point calculation',
                   comments='')
        print('xyz successfully created at:', directory_new + '\\'  + method + species + '_' +
                   str(int(round(angles_deg[i], 5))) + '_' + decimal_places +
                   '.xyz')


def calculate_rot_barrier(cwd, opt_output_file):
    '''
    The function returns the rotational barrier in different units. For each
    structure (optimized and single point) the energy of the system is extracted
    from the respective orca output file. The energy of the optimized structure
    is the smallest by definition. The single point structure with the smallest
    energy is considered as the structure of the transition state of the
    rotation. Its energy difference to the optimized structure energy is
    interpreted as rotational barrier.

    Parameters
    ----------
    cwd : string
        The whole path to the directory containing the single point calculation
        output folders (also the SP calculation run with the optimized struc-
        ture.
    opt_output_file : string
        Name of the optimization output file (with extension).

    Returns
    -------
    barrier_cm : float
        Height of the minimum rotational barrier in cm^-1
    barrier_GHz : float
        Height of the minimum rotational barrier in GHz
    barrier_Hartree : float
        Height of the minimum rotational barrier in Hartree
    min_SP : string
        Name of the single point output file (for a certain method) which 
        yields the smallest rotational barrier.
    max_SP : string
        Name of the single point output file (for a certain method) which 
        yields the highest rotational barrier.
    barrier_max_cm : float
        Height of the maximum rotational barrier in cm^-1
    SP_energy : list of floats
        Lists height of the minimum rotational barrier in cm^-1 for each method
    opt_energy : float
        Molecule energy from the geometry optimization
    output_files : list of strings
    '''
    
    output_files = []
    SP_energy = []
    
    # Read in all SP output files
    for path in Path(cwd).rglob('*.out'):
        if 'slurm' not in path.name:
            if '-OPT' not in path.name:
                if 'Ignored Calculations' not in path.name: 
                    output_files.append(path.resolve())
    
    # Extract single point energy in Hartree of each structure (= each angle)
    for output_file in output_files:
        with open(output_file) as file:
            lines = file.readlines()
        
        check = 0
        for line in reversed(lines):
            if 'FINAL SINGLE POINT ENERGY' in line:
                SP_energy.append(float(line.split()[-1]))
                check = 1
                break
        if check == 0:
            print('''"FINAL SINGLE POINT ENERGY" not found in SP output file! :( ''')
        
    # print('single point energies: ',SP_energy)

    # Extract the structure / angle with the minimum (most negative) energy. 
    # The optimization energy should be smaller than the SP energies, so the 
    # energy difference to the smallest SP energy must correspond to the
    # smallest rotational barrier
    
    min_energy_idx = min(range(len(SP_energy)), key=SP_energy.__getitem__)
    min_SP = str(output_files[min_energy_idx]).split('\\')[-1]

    # Also extract the structure / angle with the maximum (least negative) energy for comparison
    max_energy_idx = max(range(len(SP_energy)), key=SP_energy.__getitem__)
    max_SP = str(output_files[max_energy_idx]).split('\\')[-1]

    
    # Extract energy of optimized structure from corresponding SP calculation
    with open(cwd + '\\' + opt_output_file[:-4] + '\\' + opt_output_file) as opt_SP:
        output_file = opt_SP.readlines()
        
        # iterate through outputfile lines in reversed direction. The first 
        # time we find a 'FINAL SINGLE POINT ENERGY' line will contain the 
        # single point energy we are looking for.
        for line in reversed(output_file):
            if 'FINAL SINGLE POINT ENERGY' in line:
                opt_energy = float(line.split()[-1])
                break
    
    # Determine rotational barrier as min(SP) - opt difference
    barrier_Hartree = SP_energy[min_energy_idx] - opt_energy # in Hartree
    barrier_max_Hartree = SP_energy[max_energy_idx] - opt_energy
    barrier_GHz = barrier_Hartree / (1.51982850071586E-07)
    barrier_cm = barrier_Hartree / (1.51982850071586E-07) * 10**9 / (sc.c *10**2)
    barrier_max_cm = barrier_max_Hartree / (1.51982850071586E-07) * 10**9 / (sc.c *10**2)
    
    return barrier_cm, barrier_GHz, barrier_Hartree, min_SP, max_SP, barrier_max_cm, SP_energy, opt_energy, output_files




def calc_int_angular_momentum(H2_bond, arom_plane_coeffs):
    '''
    The function calculates the angular momentum P for H2 internal rotation in 
    a complex.

    Parameters
    ----------
    H2_bond: 1D numpy array of floats,
        Represents the bond vector of H2 in cartesian coordinates.
    
    arom_plane_coeffs : list of floats
        Floats represent the resulting values of the fit parameters. The plane 
        is a function of two dimensions. Its values represent the third dimen-
        sion.
        
        coeff[0] represents the derivative of the function with respect to the 
        first dimension. 
        
        coeff[1] represents the derivative of the function with respect to the 
        second dimension. 
        
        coeff[2] represents the offset of the function in the third dimension.

    Returns
    -------
    n_plane : 1D array of floats
        Floats in the array represent the components of a not-normalized normal
        vector to the aromatic plane.
    P : 1D array of floats
        Floats in the array represent the components of a not-normalized 
        vector. This vector represents the direction of the H2's internal 
        rotation angular momentum.

    '''
    
    
    # define direction vectors of aromatic plane:
    dx = np.array([1, 0, arom_plane_coeffs[0]])
    dy = np.array([0, 1, arom_plane_coeffs[1]])    
    
    # define normal vector of aromatic plane
    n_plane = np.cross(dx, dy)
    
    # define angular momentum vector of H2 as normal vector of the plane which 
    # is spanned by n_plane and the H2 bond vector:
    P = np.cross(n_plane, H2_bond)
    
    return n_plane, P
    


def xyz_abc_rot_matrix(xyz_coords, masses):
    '''
    The function reads in the cartesian coordinates of the atoms in a molecule 
    and translates and rotates them to the principle axes system (PAS). It re-
    turns the coordinates in the PAS as well as the rotation matrix.

    Parameters
    ----------
    xyz_coords : 2D np.array or list of floats
        A sublist contains three floats representing the cartesian coordinates
        of an atom in the system.
    masses : 1D array of floats
        Atomic masses with elements sorted in the same way as the xyz_coords
        array.

    Returns
    -------
    abc_coords : 2D numpy array of floats
        Each row contains three floats representing the PAS coordinates of an 
        atom in the system (molecule or complex).
    Rot_matrix : 2D numpy array of floats
        Rotates .

    '''

    CoM = np.zeros(3)
    CoM[0] = np.dot(masses, xyz_coords.T[0]) / sum(masses)
    CoM[1] = np.dot(masses, xyz_coords.T[1]) / sum(masses)
    CoM[2] = np.dot(masses, xyz_coords.T[2]) / sum(masses)
    xyz_CoM_centrd = np.transpose(xyz_coords - CoM)
    
    I_xyz = np.zeros((3, 3))
    I_abc2 = np.zeros((3, 3))

    # calculate off-diagonal elements first (also diag elements but we will
    # overwrite them soon)
    for i in range(3):
        for j in range(3):
            I_xyz[i][j] = -sum(masses * xyz_CoM_centrd[i] * xyz_CoM_centrd[j])

    # recalculate now the diagonal elements
    d_CoM_squared = xyz_CoM_centrd[0]**2 + xyz_CoM_centrd[1]**2 + xyz_CoM_centrd[2]**2
    
    for i in range(3):
        I_xyz[i][i] = sum(masses * (d_CoM_squared - xyz_CoM_centrd[i]**2))
        

    I_eigvals, Rot_matrix_np = np.linalg.eigh(I_xyz)
    I_abc = np.array([[I_eigvals[0],            0,           0],
                      [           0, I_eigvals[1],           0],
                      [           0,            0, I_eigvals[2]]])
    
    I_xyz_inv = np.linalg.inv(I_xyz)
    
    # With np.linalg.eigh() numpy also computes Rot_max but it differs from the
    # rotation matrix which can be obtained from the inversion matrix of I_xyz.
    # And it doesn't diagonalize I_xyz properly, therefore, I don't use it.
    Rot_matrix = np.dot(I_xyz_inv, I_abc)
    
    # Use the rotation matrix to rotate the center-of-mass-centered molecular 
    # axes system to the principle axes system:
    abc_coords = np.dot(Rot_matrix, xyz_CoM_centrd)
    
    
    return abc_coords.T, Rot_matrix



# convert A, B and C rotational constants in MHz to BJ, BK and B- as they are 
# used in XIAM
def rot_constants_XIAM_pickett(rot_constants):
    '''
    Depending on the input (0 or 1), the function converts pickett A, B, C 
    rotational constants to XIAM rotational constants (BJ, BK, and B-) or vice 
    versa and prints the result.

    Parameters
    ----------
    rot_constants : 1D numpy array of floats
        Pickett rotational constants in MHz or XIAM rotational constants in GHz

    Returns
    -------
    None.

    '''
    print('A, B, C -> BJ, BK, B- (0) or BJ, BK, B- -> A, B, C (1) ?')
    translation = input()
    
    if translation == '0':
        A = rot_constants[0]
        B = rot_constants[1]
        C = rot_constants[2]
    
        BJ = 0.5 * (B + C)
        BK = A - 0.5 * (B + C)
        B_minus = 0.5 * (B - C)
        print('')
        print('Convert rotation constants from Pickett to rotational constants for Xiam:')
        print('BJ = ',BJ / 10**3,'GHz')
        print('BK = ',BK / 10**3,'GHz')
        print('B_minus = ',B_minus / 10**3,'GHz')
        print('')
        
    elif translation == '1':
        BJ = rot_constants[0]
        BK = rot_constants[1]
        B_minus = rot_constants[2]
        
        A = BJ + BK
        B = B_minus + BJ
        C = BJ - B_minus
        print('')
        print('Convert rotation constants from XIAM to rotational constants for Pickett:')
        print('A = ',A * 10**3,'MHz')
        print('B = ',B * 10**3,'MHz')
        print('C = ',C * 10**3,'MHz')
        print('')



def calc_delta_and_epsilon(Rot_matrix, n_plane, P):
    '''
    The function derives the delta and epsilon value from the predicted struc-
    ture. Delta and epsilon are parameters in the Coriolis coupling terms of
    the rotational Hamiltonian used by XIAM. Delta represents the angle between
    the H2 rotational angular momentum and the a-axis. Epsilon equals the angle
    between H2 angular momentum projection to the bc plane and the b-axis.

    Parameters
    ----------
    Rot_matrix : 2D numpy array of floats
        Rotation matrix which rotates the cartesian coordinate system obtained
        from the calculation to the principle axis system.
    n_plane : 1D numpy array of floats
        Vector normal to fitted plane.
    P : 1D numpy array of floats
        Vector parallel to the angular momentum vector of H2 internal rotation.

    Returns
    -------
    delta_a : float
        Angle in rad between P and a_axis.
    epsilon_xy : float
        Angle in rad between the projection of P to the bc-plane (P_xy) and the
        b axis.

    '''
    # XIAM calls the principal axes x, y, and z. Dictionary:
    # a-axis = z-axis
    # b-axis = x-axis
    # c-axis = y-axis

    a_axis = np.dot(Rot_matrix, np.array([1, 0, 0]))
    b_axis = np.dot(Rot_matrix, np.array([0, 1, 0]))
    c_axis = np.dot(Rot_matrix, np.array([0, 0, 1]))

    # normalize n_plane:
    n_plane_unit = n_plane / np.linalg.norm(n_plane)

    # normalize P:
    P_unit = P / np.linalg.norm(P)

    # calculate the angle in rad between the z-axis (a-axis) and P:
    delta_a = np.arccos(np.clip(np.dot(a_axis, P_unit), -1.0, 1.0))

    # project P to xy plane (P_xy). Therefor, project P to the z-axis (a-axis)
    # and subtract the result from P. 
    P_xy = P - np.dot(P, a_axis) / np.linalg.norm(a_axis)**2 * a_axis

    # calculate the angle in rad between P_xy and the x-axis (b-axis):
    epsilon_xy = np.arccos(np.clip(np.dot(P_xy, b_axis), -1.0, 1.0))

    return delta_a, epsilon_xy

# print(505.379/0.278718)

