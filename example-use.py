# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 14:10:49 2021

@author: Robin Dohmen
Contact mdohmen1@gwdg.de

This code is part of the Geometry library for python. It demonstrates the usage
of the Geometry class and several extensions.
"""

# Import the necessary packages
import sys
import numpy as np

from pathlib import Path

"""
Add git repository path for orca_coord to path for work computer
Your personal Path to the orca-output folder needs to be put here. The way
you need to handle the '\' depends on your OS.
"""
sys.path.insert(0, 'D:\\orca-output')

# add git repository path for orca_coord to path for home computer
sys.path.insert(0, 'S:\\PhD\\orca-output')

# Import important functions from complementary files
from GeometryClass import Geometry, information_to_excel
from spfitExtension import (read_multiple_linfiles,
                                       search_for_duplicates,
                                       same_qantum_numbers)
from crestExtension import crest_xyz_to_ORCA
from strfitExtension import submit_multipe_stf_files

###########################################################################

"Print information of multiple files to an excel sheet"

###########################################################################
"""
Search for regular orca output files in the folder you want to look at
sometimes this search grabs files which aren't really the ORCA output files
like the _trj.out files, files created by the cluster schedulers and others.
These need to be excluded.
In this example each Path is appended to a list for further use. A few files
need to be excluded such as the slurm ouput files, atom and _trj files which
are created by orca
"""
#"""
files = []
for path in Path(
    'D:\\ownCloud\\Obenchain\\Research\\2cl6fbenzaldehyd\\B3LYP_2_chloro_6_fluoro_benzaldehyde\\B3LYP').rglob('*.out'):
    if 'EFG' not in path.name:
        if 'slurm' not in path.name:
            if 'atom' not in path.name:
                    files.append(path.resolve())
#"""

"""
Execute the script which reads the files, does the transformation and prints
it to excel. If not otherwise specified, it will print it into the first
folder in the files list. Another folder can be specified, otherwise it gets
saved in the first one folder in the file list.
"""

df = information_to_excel(files)


###########################################################################

"Print a shortened output file with transformed information for fitting with"
"Pickett or Pgopher"

###########################################################################

#molecule = Geometry('D:\\OwnCloud\\Obenchain\\chlorobenzaldehyde\\o-chlorobenzaldehyde\\calculations\\2-t-ClBz\\ne-complex\\Ne-2-t-ClBzA-B3LYP-D3-def2-TZVPP\\Ne-2-t-ClBzA-B3LYP-D3-def2-TZVPP.out')
#molecule.format_output()


###########################################################################

"Create an ORCA input file containing coordinates in the PSA"

###########################################################################
"""
Create an xyz file with the coordinates in the prinicpal axis system. By
default it is saved in the same folder, but the file can be specified.
The order of the atoms can also be changed. It needs to be a list of integers
with the current atom labels in the new order. (see documentation)
"""

# molecule.create_xyz_file(outputfile='my_molecule.xyz')


###########################################################################

"Create an ORCA input file containing coordinates in the PSA"

###########################################################################
# template = 'D:\\OwnCloud\\Obenchain\\Research\\fluorobenzaldehydes\\m-fluorobenzaldehyde\\calculations\\3-t-FBz\\3-t-FBz-H2\\orca 5.0.4\\3-t-FBz-H2_template.inp'
# molecule.create_templated_orca_input(template,
#                           comment='my molecule',
#                           outputfile='my-calculation')


###########################################################################

"Create internal coordinate file for strfit"

###########################################################################
"""
Define a connection matrix for the internal coordinates that is to your liking
for fitting the parameters you need. It needs the atom labels for the
connecting atom in the first column, the third atom to form an angle in the
second column and the third atom for the formation of an dihedral angle in the
third column. It gets printed as ususal.
"""
connection_matrix = np.array([[0, 0, 0],
                              [1, 0, 0],
                              [2, 1, 0],
                              [3, 2, 1],
                              [1, 2, 3],
                              [1, 2, 3],
                              [2, 1, 5],
                              [3, 2, 1],
                              [4, 3, 2],
                              [4, 3, 2]])

# molecule.create_interal_coordinates(connection_matrix)


###########################################################################

"Compare multiple lin files from pickett, seaching for frequencies which were"
"assingend multiple times in different lin files"

###########################################################################
"""
Read all .lin files in directory and it's subdirectories. These are put in a
dictionay which can be given to multiple functions for different comparisons.
Search_for_duplicates shows all frequencies which were doubly assigned in
several .lin files.

Same_quantum_numbers looks for transitions with the same quantum numbers which
were assigned in both files eg. for the purpose of determing relative
populations.
"""

#lin_dict = read_multiple_linfiles(
#    'S:\\PhD\\Owncloud2\\PhD\\assignment\\Benzaldehydes\\4-chlorobenzaldehyde')

#doubles = search_for_duplicates(lin_dict)
#qn = same_qantum_numbers(lin_dict, '4-Cl35', '4-13C6')


###########################################################################

"Convert crest confomers to ORCA input"

###########################################################################
"""
Function converts crest confomers in a given folder into input files for ORCA.
The function reads the energy of the confomer in the folder to let the user
determine a cutoff value or a number of low energy confomers to use. The
geometry is read and an input file created for each confomer in a new folder.
A default command line with the ORCA command
!B3LYP D3 def2-TZVP VeryTightOpt VeryTightSCF Mass2016
is used but can be changed beforehand when calling the function.
"""
#%%
template = 'D:\\OwnCloud\\PhD\\coding\\TEMPO_HFB_02.inp'
folder = 'D:\\OwnCloud\\Obenchain\\Research\\fluorobenzaldehydes\\m-fluorobenzaldehyde\\calculations\\3-t-FBz\\3-t-FBz-H2\\crest'
crest_xyz_to_ORCA(folder, template, XYZfile=True)

#%%
files = []
for path in Path(
    'D:\\OwnCloud\\Obenchain\\Research\\fluorobenzaldehydes\\m-fluorobenzaldehyde\\calculations\\3-t-FBz\\3-t-FBz-H2\\crest\\orca_input').rglob('*.xyz'):
    files.append(path.resolve())

df = information_to_excel(files)


# %%
###########################################################################

"Create a strfit file from template"

###########################################################################
"""
This function is primarily designed for submiting a number of strfit jobs using
different geometries as starting guesses. An strfitfile needs to be specified
which contains the desired fitting parameters.
The starting guesses should be xyz files or orca ouput files with a geometry
of the same molecule. The function reads each geometry, calculates the internal
coordinates based on the ones used in the template. Therefore the order of the
atom and the geometry file should be the same or the correct order conversion
to the strfit atom order needs to be supplied. Based on the template a strfit
file with the same fitting parameters is created and submitted.
The function 'information to excel' can also recognize a list of strfit outputs
and put these into an excel sheet.
"""
strfitfile = 'D:\\OwnCloud\\Obenchain\\Research\\fluorobenzaldehydes\\p-fluorobenzaldehyde\\STRFIT\\3 DOF 01\\temp_xyz\\4-FBz-(o-H2)-template.stf'

files = []
for path in Path(
             'D:\\OwnCloud\\Obenchain\\Research\\fluorobenzaldehydes\\p-fluorobenzaldehyde\\STRFIT\\3 DOF 01').rglob('*.out'):
    if "strfit" not in path.name:
                  files.append(path.resolve())
for file in files:
    mol = Geometry(file)
    mol.remove_atoms([15, 16])
    H2COM = mol.compute_COM()
    mol = Geometry(file)
    mol.remove_atoms(np.arange(1, 15))
    FBzCOM = mol.compute_COM()
    mol = Geometry(file)
    mol.add_atoms(['x'], H2COM)
    mol.add_atoms(['x'], FBzCOM)
    mol.reorder_coordinates([16, 17, 15, 18, 6, 1, 2, 3, 4, 5, 7, 8, 9, 11, 10, 13, 12, 14])
    mol.create_xyz_file()
files = []
for path in Path(
             'D:\\OwnCloud\\Obenchain\\Research\\fluorobenzaldehydes\\p-fluorobenzaldehyde\\STRFIT\\3 DOF 01').rglob('*.xyz'):
    if 'planar-scan' in path.name:
        if 'strfit' not in path.name:
                  files.append(path.resolve())
submit_multipe_stf_files(strfitfile, files)

# files = []
# for path in Path(
#         'D:\\OwnCloud\\PhD\\calculations\\chlorobenzaldehydes\\4-ClBz\\monomer\\').rglob('*.out'):
#     if 'strfit-' not in path.name:
#        if 'strfit' in path.name:
#             files.append(path.resolve())

# information_to_excel(files, 'D:\\OwnCloud\\PhD\\calculations\\chlorobenzaldehydes\\4-ClBz\\monomer\\strf_restult.xlsx')

