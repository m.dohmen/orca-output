# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 12:52:59 2023

@author: kempken1
"""

import sys
sys.path.insert(0, 'D:\\OwnCloud\\Beate\\Ready to share')
from XIAM_extension import run_XIAM, read_XIAM_output, systematic_XIAM_fitting, rot_const_converter

###############################################################################
###############################################################################

" Run XIAM "
" -> run_XIAM(cwd, xi_name, xo_name) "

# Example use of the function
# run_XIAM('D:\\OwnCloud\\Research\\fluorobenzaldehydes\\o-fluorobenzaldehyde\\XIAM\\p-H2_o-H2_cav-o-H2\\Without 7763 and 12282 MHz, new epsilon and delta prediction', '2-t-FBz_XIAM.xi', '2-t-FBz_XIAM_fit.xo')

###############################################################################
###############################################################################

" Read XIAM fit output "
" -> read_XIAM_output(cwd, xo_name, params) "

###############################################################################
'''
The function opens a XIAM output fit file (not prediction file) and returns
the fit results.
'''

# Example use of the function
# params_val, params_err, err_div_val, terminator, std_dev, [BJ, BK, B_minus] = read_XIAM_output('D:\\OwnCloud\\Research\\fluorobenzaldehydes\\m-fluorobenzaldehyde\\XIAM\\3-c-FBz\\p-H2_o-H2_cav-o-H2', '3-c-FBz_XIAM_fit.xo', ['BJ', 'BK', 'B-', 'DJK', 'DK', 'dj', 'dk', 'V1n', 'Dc3J', 'Dpi2J', 'Dpi2K', 'Dpi2-', 'epsil', 'delta'])

###############################################################################
###############################################################################

" Try XIAM fits with different parameter combinations "
" -> systematic_XIAM_fitting(cwd, xi_name, params, predicted_params, rel_err_limit, std_dev_max, tuple_sizes = [0], problematic_combs = [])"


# Example use of the function
# systematic_XIAM_fitting('D:\\OwnCloud\\Research\\fluorobenzaldehydes\\o-fluorobenzaldehyde\\XIAM\\only p-H2',
#                         '2-t-FBz_XIAM.xi', ['DJ', 'DK', 'V1n', 'delta'], ['V1n', 'delta'], 0.1, 0.1)

###############################################################################
###############################################################################

" Translation between rotational constants from pickett to XIAM and vice versa "
" -> rot_const_converter(rot_constants, conv) "


# Example use of the function
# a, b, c = rot_const_converter([3,5,2], conv = 'XIAM_to_Pickett')
